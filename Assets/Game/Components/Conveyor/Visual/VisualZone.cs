using System;
using System.Collections.Generic;
using UnityEngine;


public sealed class VisualZone : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> items;

    private int count;

    public void Setup(int currentAmount)
    {
        this.count = Math.Min(currentAmount, this.items.Count);

        for (var i = 0; i < currentAmount; i++)
        {
            var item = this.items[i];
            item.SetActive(true);
        }

        var count = this.items.Count;
        for (var i = currentAmount; i < count; i++)
        {
            var item = this.items[i];
            item.SetActive(false);
        }
    }

    public void AddItems(int range)
    {
        var previousValue = this.count;
        var newValue = Math.Min(previousValue + range, this.items.Count);
        this.count = newValue;

        for (var i = previousValue; i < newValue; i++)
        {
            var item = this.items[i];
            item.SetActive(true);
        }
    }

    public void RemoveItems(int range)
    {
        var previousValue = this.count;
        var newValue = Math.Max(previousValue - range, 0);
        this.count = newValue;

        for (var i = newValue; i < previousValue; i++)
        {
            var item = this.items[i];
            item.SetActive(false);
        }
    }

    //#if UNITY_EDITOR
    //        [Button("Setup")]
    //        private void Editor_Setup()
    //        {
    //            this.items = new List<GameObject>();
    //            foreach (Transform child in this.transform)
    //            {
    //                this.items.Add(child.gameObject);
    //            }
    //        }
    //#endif
}
