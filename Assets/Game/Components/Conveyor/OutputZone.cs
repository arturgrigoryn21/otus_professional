﻿using Entities;
using System;
using UnityEngine;

[Serializable]
class OutputZone
{
    [SerializeField]
    private MonoEntity outputZone;

    [SerializeField]
    private int amount;

    public bool IsFull=> outputZone.Element<LoadComponent>().IsFull;

    private LoadComponent loadComponent;
    private LoadComponent LoadComponent => loadComponent ?? (loadComponent = outputZone.Element<LoadComponent>());
    public void LoadResources()
    {
        LoadComponent.LoadResources(amount);
    }
}
