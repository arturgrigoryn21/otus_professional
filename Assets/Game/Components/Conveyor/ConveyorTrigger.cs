using Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorTrigger : MonoBehaviour, IGetZonaInfo
{
    [SerializeField]
    private MonoEntity entity;
    private ZoneInfo zoneInfo;
    public IEntity Conveyor => entity;
    public ZoneType CurrentZone => zoneInfo.ZoneType;

    public void GetZoneInfo(ZoneInfo zoneInfo)
    {
        this.zoneInfo = zoneInfo;
    }
}
