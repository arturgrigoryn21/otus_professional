using LimitedNumbers;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entities;

public class ConveyorWork : MonoBehaviour
{
    [SerializeField]
    private TimerScript timerScript;

    [SerializeField]
    private OutputZone outputZone;

    [SerializeField]
    private List<InputZone> inputZones;


    private void OnEnable()
    {
        foreach (var zone in inputZones)
            timerScript.OnStarted += zone.UnloadResources;

        timerScript.OnFinished += outputZone.LoadResources;
    }

    private void OnDisable()
    {
        foreach (var zone in inputZones)
            timerScript.OnStarted -= zone.UnloadResources;

        timerScript.OnFinished -= outputZone.LoadResources;
    }
    void Update()
    {
        if (timerScript.IsPlaying)
            return;

        if (inputZones.Any(zone => !zone.CheckAmount()))
            return;

        if (outputZone.IsFull)
            return;

        timerScript.Play();
    }
}
