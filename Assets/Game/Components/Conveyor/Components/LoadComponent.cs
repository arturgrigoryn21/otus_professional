
using LimitedNumbers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadComponent : MonoBehaviour, IGetZonaInfo
{
    [SerializeField]
    private MonoLimitedNumber monoLimitedNumber;
    private ZoneInfo zoneInfo;
    public bool IsFull => monoLimitedNumber.IsLimit;   
    public ResourceType ResourceType => zoneInfo.ResourceType;
    public int FreeSlots => monoLimitedNumber.AvailableRange;
    public int CurrentAmount => monoLimitedNumber.Value;

    public void LoadResources(int value)
    {
        monoLimitedNumber.Increment(value);
    }
    public void GetZoneInfo(ZoneInfo zoneInfo)
    {
        this.zoneInfo = zoneInfo;
    }
}
