﻿
using LimitedNumbers;
using UnityEngine;

public class UnloadComponent : MonoBehaviour, IGetZonaInfo
{
    [SerializeField]
    private MonoLimitedNumber monoLimitedNumber;
    private ZoneInfo zoneInfo;
    public bool IsEmpty => monoLimitedNumber.IsZero;
    public ResourceType ResourceType => zoneInfo.ResourceType;
    public int CurrentAmount => monoLimitedNumber.Value;
    public int UnloadAllResources()
    {
        var all = monoLimitedNumber.Value;
        monoLimitedNumber.Decrement(all);
        return all;
    }

    public void UnloadResources(int range)
    {
        monoLimitedNumber.Decrement(range);
    }

    public void GetZoneInfo(ZoneInfo zoneInfo)
    {
        this.zoneInfo = zoneInfo;
    }
}
