﻿using Entities;
using System;
using UnityEngine;

[Serializable]

class InputZone 
{ 
    [SerializeField]
    private int amount;

    [SerializeField]
    private MonoEntity inputZone;

    private UnloadComponent unloadComponent;
    private UnloadComponent UnloadComponent => unloadComponent ?? (unloadComponent = inputZone.Element<UnloadComponent>());

    public bool CheckAmount()
    {
        if (UnloadComponent.CurrentAmount >= amount)
            return true;
        return false;
    }
    public void UnloadResources()
    {
        UnloadComponent.UnloadResources(amount);
    }    
}
