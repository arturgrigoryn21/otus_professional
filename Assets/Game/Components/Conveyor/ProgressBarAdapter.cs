using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressBarAdapter : MonoBehaviour
{
    [SerializeField]
    private TimerScript timerScript;

    [SerializeField]
    private ProgressBar progressBar;


    private void OnEnable()
    {
        timerScript.OnTimeChanged += TimerScript_OnTimeChanged;
        timerScript.OnStarted += TimerScript_OnStarted;
        timerScript.OnFinished += TimerScript_OnFinished;
    }

    private void TimerScript_OnFinished()
    {
        progressBar.SetVisible(false);
    }

    private void TimerScript_OnStarted()
    {
        progressBar.SetVisible(true);
    }

    private void OnDisable()
    {
        timerScript.OnTimeChanged -= TimerScript_OnTimeChanged;
        timerScript.OnStarted -= TimerScript_OnStarted;
        timerScript.OnFinished -= TimerScript_OnFinished;
    }
    private void TimerScript_OnTimeChanged()
    {
        progressBar.SetProgress(timerScript.Progress);
    }
}
