using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LimitedNumbers;

public class ConveyorAdapter : MonoBehaviour
{
    [SerializeField]
    private VisualZone visualZone;

    [SerializeField]
    private MonoLimitedNumber limitedNumber;

    private void OnEnable()
    {
        limitedNumber.OnValueSetuped += Setup;
        limitedNumber.OnValueIncremented += Incremented;
        limitedNumber.OnValueDecremented += Decremented;        
    }
    private void OnDisable()
    {
        limitedNumber.OnValueSetuped -= Setup;
        limitedNumber.OnValueIncremented -= Incremented;
        limitedNumber.OnValueDecremented -= Decremented;
    }
    private void Setup(int value)
    {
        visualZone.Setup(value);
    }
    
    private void Decremented(int previousValue, int newValue)
    {
        var value = previousValue - newValue;
        visualZone.RemoveItems(value);
    }

    private void Incremented(int previousValue, int newValue)
    {
        var value = newValue - previousValue;
        visualZone.AddItems(value);
    }


#if UNITY_EDITOR
    private void OnValidate()
    {
        limitedNumber.OnValueSetuped += Setup;
        limitedNumber.OnValueSetuped -= Setup;
    }
#endif
}
