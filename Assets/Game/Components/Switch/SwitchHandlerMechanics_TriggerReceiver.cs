﻿using UnityEngine;

public class SwitchHandlerMechanics_TriggerReceiver : MonoBehaviour
{
    [SerializeField]
    public TriggerReceiver triggerReceiver;

    public void SwitchOn()
    {
        triggerReceiver.IsEnable = true;
    }

    public void SwitchOff()
    {
        triggerReceiver.IsEnable = false;
    }
}
