﻿using UnityEngine;

public class SwitchHadlerMechanics_Collider : MonoBehaviour
{
    [SerializeField]
    private GameObject _collider;

    public void SwitchOn()
    {
        _collider.SetActive(true);
    }

    public void SwitchOff()
    {
        _collider.SetActive(false);
    }
}
