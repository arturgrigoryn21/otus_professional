﻿using System;

public interface ISwitchComponent
{
    bool IsSwitch { get; }
    event Action OnSwitchOn;
    event Action OnSwitchOff;
    void SwitchOn();
    void SwitchOff();
}
