using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchComponent : MonoBehaviour, ISwitchComponent
{
    public bool IsSwitch => switchMechincs.IsSwitch;

    public event Action OnSwitchOn
    {
        add { switchMechincs.OnSwitchOn += value; }
        remove { switchMechincs.OnSwitchOn -= value; }
    }
    public event Action OnSwitchOff
    {
        add { switchMechincs.OnSwitchOff += value; }
        remove { switchMechincs.OnSwitchOff -= value; }
    }

    [SerializeField]
    private SwitchMechincs switchMechincs;
    public void SwitchOff()
    {
        switchMechincs.SwitchOff();
    }

    public void SwitchOn()
    {
        switchMechincs.SwitchOn();
    }
}
