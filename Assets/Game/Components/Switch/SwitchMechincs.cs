﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class SwitchMechincs : MonoBehaviour
{
    public bool IsSwitch { get; internal set; }
    public event Action OnSwitchOn;
    public event Action OnSwitchOff;

    [SerializeField]
    private UnityEvent onSwitchOff;

    [SerializeField]
    public UnityEvent onSwitchOn;

    internal void SwitchOff()
    {
        OnSwitchOff?.Invoke();
        onSwitchOff?.Invoke();
        IsSwitch = false;
    }

    internal void SwitchOn()
    {
        OnSwitchOn?.Invoke();
        onSwitchOn?.Invoke();
        IsSwitch = true;
    }
}
