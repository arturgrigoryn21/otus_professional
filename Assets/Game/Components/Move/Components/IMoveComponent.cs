﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Interfaces
{
    interface IMoveComponent
    {
        event Action OnMovingEvent;
        void MoveByKey(Vector3 direction);
        void MoveByMouse(Vector3 direction);
    }
}
