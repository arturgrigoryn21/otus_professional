using Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class MoveComponent : MonoBehaviour, IMoveComponent
    {
        public event Action OnMovingEvent
        {
            add { moveMechanics.OnMovingEvent += value; }
            remove { moveMechanics.OnMovingEvent -= value; }
        }

        [SerializeField]
        private MoveMechanics moveMechanics;

        public void MoveByKey(Vector3 direction)
        {
            moveMechanics.Move(direction);
        }

        public void MoveByMouse(Vector3 direction)
        {
            throw new NotImplementedException();
        }
    }
}
