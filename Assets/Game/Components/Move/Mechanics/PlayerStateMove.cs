﻿using Assets.Game.Components.Speed;
using UnityEngine;


namespace Player
{
    public class PlayerStateMove : State
    {
        [SerializeField]
        private Transform rootTransform;

        [SerializeField]
        private MoveMechanics moveMechanics;

        [SerializeField]
        private SpeedMechanics speedMechanics;
        public override void UpdateState(float values)
        {
            if (moveMechanics.IsMoving)
                Moving();
        }

        private void Moving()
        {
            rootTransform.Translate(moveMechanics.Direction * speedMechanics.Speed * Time.fixedDeltaTime);
        }
    }
}


