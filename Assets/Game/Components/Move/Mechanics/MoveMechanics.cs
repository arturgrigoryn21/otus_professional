﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Player
{
    class MoveMechanics : MonoBehaviour
    {
        public event Action OnMovingEvent;
        public event Action OnFinishMovingEvent;

        public bool IsMoving => isMoving;
        public Vector3 Direction => direction;

        private bool isMoving;
        private bool finishMove;
        private Vector3 direction;

        
        public void Move(Vector3 direction)
        {
            this.direction = direction;
            this.finishMove = false;

            if (!this.isMoving)
            {
                this.isMoving = true;
                OnMovingEvent?.Invoke();
            }
        }

        private void Update()
        {
            if (!this.isMoving)
            {
                return;
            }

            if (this.finishMove)
            {
                this.isMoving = false;
                OnFinishMovingEvent?.Invoke();
            }

            this.finishMove = true;
        }

    }
}
