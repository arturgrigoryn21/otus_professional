﻿using UnityEngine;



namespace Player
{
    public class PlayerStateMoveHandler : MonoBehaviour
    {
        [SerializeField]
        private MoveMechanics moveMechanics;

        [SerializeField]
        private PlayerStateMachine playerStateMachine;

        private void OnEnable()
        {
            moveMechanics.OnMovingEvent += MoveState;
            moveMechanics.OnFinishMovingEvent += IdleState;
        }

        private void OnDisable()
        {
            moveMechanics.OnMovingEvent -= MoveState;
            moveMechanics.OnFinishMovingEvent -= IdleState;
        }
        private void MoveState()
        {
            playerStateMachine.EnterState(PlayerState.Move);
        }

        private void IdleState()
        {
            playerStateMachine.EnterState(PlayerState.Idle);
        }
    }
}