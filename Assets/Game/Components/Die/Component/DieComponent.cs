﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Interfaces;

namespace Player
{
    class DieComponent : MonoBehaviour, IDieComponent
    {
        [SerializeField]
        private DieMechanics dieMechanics;

        public event Action Died
        {
            add { dieMechanics.DeathEvent += value; }
            remove { dieMechanics.DeathEvent -= value; }
        }


    }
}
