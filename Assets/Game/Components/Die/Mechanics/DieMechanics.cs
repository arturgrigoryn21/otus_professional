using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class DieMechanics : MonoBehaviour
    {
        [SerializeField]
        private GameObject parentObject;
        public event Action DeathEvent;

        public void CheckDeath(int health)
        {
            if (health == 0)
            {
                Die();
                DeathEvent?.Invoke();               
            }
        }

        private void Die()
        {
            parentObject.SetActive(false);
        }
    }
}
