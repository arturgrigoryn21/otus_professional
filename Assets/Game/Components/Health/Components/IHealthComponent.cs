﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    interface IHealthComponent
    {
        event Action<int> HealthChanged;

        void SetHealth(int health);
    }
}
