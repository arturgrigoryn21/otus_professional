using Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class HealthComponent : MonoBehaviour, IHealthComponent
    {
        [SerializeField]
        private HealthMechanics healthMechanics;

        public event Action<int> HealthChanged
        {
            add { healthMechanics.HealthChanged += value; }
            remove { healthMechanics.HealthChanged += value; }
        }

        public void SetHealth(int health)
        {
            healthMechanics.SetHealth(health);
        }
    }
}
