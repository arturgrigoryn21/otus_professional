﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Player
{
    class HealthMechanics : MonoBehaviour
    {
        [SerializeField]
        private int maxHP = 25;
        private int health;

        public event Action<int> HealthChanged;

        [SerializeField]
        private UnityEvent<int> unityHealthChanged;

        private void Start()
        {
            this.health = maxHP;
        }
        public void IncrementHealth(int range)
        {
            health = Math.Min(health += range, maxHP);
            HealthChanged?.Invoke(health);
            unityHealthChanged?.Invoke(health);
        }

        internal void SetHealth(int health)
        {
            this.maxHP = health;
        }

        public void DecrementHealth(int range)
        {
            health = Math.Max(health -= range, 0);
            HealthChanged?.Invoke(health);
            unityHealthChanged?.Invoke(health);
        }

    }
}
