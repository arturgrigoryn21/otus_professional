using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerReceiver : MonoBehaviour
{
    public Action<Collider> OnTriggerEnterEvent;
    public Action<Collider> OnTriggerExitEvent;
    public Action<Collider> OnTriggerStayEvent;

    public bool IsEnable { get; set; } = true;
    private void OnTriggerEnter(Collider other)
    {
        if (!IsEnable)
            return;
        OnTriggerEnterEvent?.Invoke(other);
    }

    private void OnTriggerExit(Collider other)
    {
        if (!IsEnable)
            return;
        OnTriggerExitEvent?.Invoke(other);
    }

    private void OnTriggerStay(Collider other)
    {
        if (!IsEnable)
            return;
        OnTriggerStayEvent?.Invoke(other);
    }
}
