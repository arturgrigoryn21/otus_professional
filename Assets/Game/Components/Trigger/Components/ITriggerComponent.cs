﻿using System;
using UnityEngine;

namespace Interfeces
{
    interface ITriggerComponent
    {
        event Action<Collider> OnTriggerEnter;
        event Action<Collider> OnTriggerExit;
        event Action<Collider> OnTriggerStay;
    }
    
}
