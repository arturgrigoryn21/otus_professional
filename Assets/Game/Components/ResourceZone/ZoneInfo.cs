
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneInfo : MonoBehaviour
{
    [SerializeField]
    private ZoneType zoneType;

    [SerializeField]
    private ResourceType resourceType;

    public ZoneType ZoneType  => zoneType;
    public ResourceType ResourceType => resourceType;

    private void Start()
    {
        foreach(var childer in GetComponentsInChildren<IGetZonaInfo>())
        {
            childer.GetZoneInfo(this);
        }
    }
}
