﻿using UnityEngine;

namespace Assets.Game.Components.Damage
{
    class DamageComponent : MonoBehaviour, IDamageComponent
    {
        [SerializeField]
        private DamageMechanics damageMechanics;
        public int Damage => damageMechanics.Damage;

        public void SetDamage(int dmg)
        {
            damageMechanics.SetDamage(dmg);
        }
    }
}
