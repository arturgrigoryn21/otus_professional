﻿using UnityEngine;
using UnityEngine.Events;

namespace Assets.Game.Components.Damage
{
    class DamageMechanics : MonoBehaviour
    {
        [SerializeField]
        private int damage;
        [SerializeField]
        private UnityEvent<int> onDamageChanged;
        public int Damage => damage;

        internal void SetDamage(int dmg)
        {
            this.damage = dmg;
            onDamageChanged?.Invoke(dmg);
        }

        private void Start()
        {
            SetDamage(damage);
        }
    }
}
