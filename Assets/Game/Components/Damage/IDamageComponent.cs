﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Game.Components.Damage
{
    internal interface IDamageComponent
    {
        int Damage { get;}
        void SetDamage(int dmg);

    }
}
