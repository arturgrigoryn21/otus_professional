﻿using Entities;
using GameSystems;
using System;
using UnityEngine;

class ConveyorBagInteractor: MonoBehaviour, IGameInitElement
{
    private IPlayerBag playerBag;

    public void PutResources(IEntity conveyor)
    {
        var conveyorComponent = conveyor.Element<LoadComponent>();
        if (conveyorComponent.IsFull)
        {
            return;
        }

        var resourceType = conveyorComponent.ResourceType;
        var resourcesInBag = this.playerBag.GetRecourcesCount(resourceType);
        if (resourcesInBag <= 0)
        {
            return;
        }

        var putAmount = Math.Min(resourcesInBag, conveyorComponent.FreeSlots);
        this.playerBag.ExtractResources(resourceType, putAmount);
        conveyorComponent.LoadResources(putAmount);
    }


    public void ExtractResources(IEntity conveyor)
    {
        var conveyorComponent = conveyor.Element<UnloadComponent>();
        if (conveyorComponent.IsEmpty)
        {
            return;
        }

        var targetResource = conveyorComponent.ResourceType;
        var collectAmount = conveyorComponent.UnloadAllResources();
        this.playerBag.PutResources(targetResource, collectAmount);
    }


    public void OnInitGame(IGameSystem gameSystem)
    {
        this.playerBag = gameSystem.GetService<IPlayerBag>();
    }
}
