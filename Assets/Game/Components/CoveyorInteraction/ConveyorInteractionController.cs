using Assets.Scripts;
using GameSystems;
using Interfeces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorInteractionController : MonoBehaviour,
    IGameInitElement, IGameStartElement, IGameFinishElement
{
    private ITriggerComponent playerTrigger;
    private ConveyorBagInteractor interactor;

    public void OnFinishGame()
    {
        playerTrigger.OnTriggerEnter -= InteractWithConveyor;
    }

    public void OnInitGame(IGameSystem gameSystem)
    {
        interactor = gameSystem.GetService<ConveyorBagInteractor>();
        playerTrigger = gameSystem.GetService<PlayerService>().Player.Element<ITriggerComponent>();
    }

    public void OnStartGame()
    {
        playerTrigger.OnTriggerEnter += InteractWithConveyor;
    }

    private void InteractWithConveyor(Collider obj)
    {
        if (obj.TryGetComponent(out ConveyorTrigger conveyor))
        {
            if (conveyor.CurrentZone == ZoneType.InputZone)
            {
                interactor.PutResources(conveyor.Conveyor);
            }
            else if (conveyor.CurrentZone == ZoneType.OutputZone)
            {
                interactor.ExtractResources(conveyor.Conveyor);
            }
        }
    }
}
