﻿using Entities;

class AttackManager
{
    private bool isAttack;
    public void Attack(IAttackComponent attackComponent, IEntity enemy)
    {
        if (isAttack)
            return;

        isAttack = true;
        attackComponent.OnFinishAttack += FinishAttack;
        attackComponent.Attack(enemy);
    }   

    public void FinishAttack()
    {
        isAttack = false;
    }
}

