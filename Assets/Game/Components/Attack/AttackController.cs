﻿using Assets.Game.Scripts;
using Assets.Scripts;
using Entities;
using GameSystems;
using Interfeces;
using UnityEngine;

class AttackController : MonoBehaviour,
    IGameStartElement, IGameFinishElement, IGameInitElement
{
    private ITriggerComponent triggerComponent;
    private IAttackComponent attackComponent;
    private AttackManager attackManager;
    public void OnFinishGame()
    {
        triggerComponent.OnTriggerEnter -= Attack;
        triggerComponent.OnTriggerStay -= Attack;
        triggerComponent.OnTriggerExit -= CancelAttack;
    }

    public void OnInitGame(IGameSystem gameSystem)
    {
        triggerComponent = gameSystem.GetService<PlayerService>().Player.Element<ITriggerComponent>();
        attackComponent = gameSystem.GetService<PlayerService>().Player.Element<IAttackComponent>();
        attackManager = new AttackManager(); 
    }

    public void OnStartGame()
    {
        triggerComponent.OnTriggerEnter += Attack;
        triggerComponent.OnTriggerStay += Attack;
        triggerComponent.OnTriggerExit += CancelAttack;
    }

    private void Attack(Collider collider)
    {
        if(IsItEnemy(collider, out IEntity enemy))
            attackManager.Attack(attackComponent, enemy);
    }

    private void CancelAttack(Collider collider)
    {
        if (IsItEnemy(collider, out IEntity enemy))
            attackManager.FinishAttack();
    }

    private bool IsItEnemy(Collider collider, out IEntity entity)
    {
        if (collider.TryGetComponent(out entity) && entity.TryElement(out ITagComponent tag) && tag.Tag == "Enemy")
        {
            return true;
        }
        return false;
                
    }
}

