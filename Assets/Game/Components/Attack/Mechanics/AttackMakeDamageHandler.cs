﻿using Entities;
using Interfaces;
using UnityEngine;

public class AttackMakeDamageHandler : MonoBehaviour
{
    private int damage;

    [SerializeField]
    private AttackMechanics attackMechanics;

    private void OnEnable()
    {
        attackMechanics.OnProvideDamage += MakeDamage;
    }

    private void OnDisable()
    {
        attackMechanics.OnProvideDamage -= MakeDamage;
    }

    public void SetDamage(int dmg)
    {
        damage = dmg;
    }
    private void MakeDamage(IEntity enemy)
    {
        var takeDmg = enemy.Element<ITakeDamageComponent>();
        takeDmg.TakeDamage(damage);
    }
}
