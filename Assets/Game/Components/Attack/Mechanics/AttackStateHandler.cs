﻿using Player;
using UnityEngine;

class AttackStateHandler : MonoBehaviour
{
    [SerializeField]
    private AttackMechanics moveMechanics;

    [SerializeField]
    private PlayerStateMachine playerStateMachine;

    private void OnEnable()
    {
        moveMechanics.OnStartAttack += AttackState;
        moveMechanics.OnFinishAttack += IdleState;
    }

    private void OnDisable()
    {
        moveMechanics.OnStartAttack -= AttackState;
        moveMechanics.OnFinishAttack -= IdleState;
    }
    private void AttackState()
    {
        playerStateMachine.EnterState(PlayerState.Attack);
    }

    private void IdleState()
    {
        playerStateMachine.EnterState(PlayerState.Idle);
    }
}

