﻿using Entities;
using System;
using UnityEngine;
using UnityEngine.Events;

class AttackMechanics : MonoBehaviour
{
    [SerializeField]
    private AnimationPlayerObserver animationAttackObserver;

    public event Action OnStartAttack;
    public event Action OnFinishAttack;
    public event Action<IEntity> OnProvideDamage;

    private IEntity currentEnemy;


    private void OnEnable()
    {
        animationAttackObserver.OnFinishAttack += FinishAttack;
        animationAttackObserver.OnDamageMoment += ProvideDamage;
    }

    private void OnDisable()
    {
        animationAttackObserver.OnFinishAttack -= FinishAttack;
        animationAttackObserver.OnDamageMoment -= ProvideDamage;
    }
    public void StartAttack(IEntity enemy)
    {
        this.currentEnemy = enemy;
        OnStartAttack?.Invoke();
    }

    private void ProvideDamage()
    {
        OnProvideDamage?.Invoke(currentEnemy);
        
    }
    public void FinishAttack()
    {      
        OnFinishAttack?.Invoke();       
    }
}
