﻿using Entities;
using System;
using UnityEngine;

class AttackComponent : MonoBehaviour, IAttackComponent
{
    [SerializeField]
    private AttackMechanics attackMechanics;

    public event Action OnFinishAttack
    {
        add { attackMechanics.OnFinishAttack += value; }
        remove { attackMechanics.OnFinishAttack -= value; }
    }

    public void Attack(IEntity enemy)
    {
        attackMechanics.StartAttack(enemy);
    }

    public void FinishAttack()
    {
        attackMechanics.FinishAttack();
    }
}

