﻿using Entities;
using System;

interface IAttackComponent
{
    event Action OnFinishAttack;
    void Attack(IEntity enemy);
    void FinishAttack();
}

