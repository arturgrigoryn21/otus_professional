﻿using System;
using UnityEngine;

class AnimationPlayerObserver : MonoBehaviour
{
    public event Action OnFinishAttack;
    public event Action OnDamageMoment;
    public void FinishAttack()
    {
        OnFinishAttack?.Invoke();
    }

    public void DamageMoment()
    {
        OnDamageMoment?.Invoke();
    }
}

