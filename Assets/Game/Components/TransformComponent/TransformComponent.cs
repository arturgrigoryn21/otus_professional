using UnityEngine;

namespace Assets.Game.Scripts.Common
{
    public class TransformComponent : MonoBehaviour, ITransformComponent
    {
        public Vector3 Position
        {
            get { return this.positionTransform.position; }
        }

        public Quaternion Rotation
        {
            get { return this.rotationTransform.rotation; }
        }

        [SerializeField]
        private Transform positionTransform;

        [SerializeField]
        private Transform rotationTransform;
    }
}