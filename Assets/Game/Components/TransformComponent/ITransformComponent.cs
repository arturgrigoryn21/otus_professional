using UnityEngine;

namespace Assets.Game.Scripts.Common
{
    public interface ITransformComponent
    {
        Vector3 Position { get; }

        Quaternion Rotation { get; }
    }
}