﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Player
{
    class InfoMechanics : MonoBehaviour
    {
        [SerializeField]
        private MoveMechanics moveMechanics;

        private bool isMoving;

        private float distance = 0;
        private Vector3 pastPoint;

        public event Action<float> DistanceChanged;
        private void Start()
        {
            pastPoint = transform.position;
            moveMechanics.OnMovingEvent += () => isMoving = true;
            moveMechanics.OnFinishMovingEvent += () => isMoving = false;
        }
        private void FixedUpdate()
        {
            if(isMoving)
                CalculateDistance();
        }

        private void CalculateDistance()
        {
            var vector = transform.position - pastPoint;
            pastPoint = transform.position;
            var currentDistance = vector.magnitude;
            distance += currentDistance;
            DistanceChanged?.Invoke(distance);
        }
    }
}
