﻿using Interfeces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Player
{
    class InfoComponent : MonoBehaviour, IInfoComponent
    {
        [SerializeField]
        private InfoMechanics infoMechanics;

        public event Action<float> OnDistanceChanged
        {
            add { infoMechanics.DistanceChanged += value; }
            remove { infoMechanics.DistanceChanged += value; }
        }
    }
}
