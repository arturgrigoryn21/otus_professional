using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Interfaces;

namespace Player
{
    public class TakeDamageComponet : MonoBehaviour, ITakeDamageComponent
    {
        [SerializeField]
        private TakeDamageMechanics takeDamageMechanics;
        public void TakeDamage(int dmg)
        {
            takeDamageMechanics.TakeDamage(dmg);
        }

    }
}
