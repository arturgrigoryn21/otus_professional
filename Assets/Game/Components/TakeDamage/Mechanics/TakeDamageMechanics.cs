﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace Player
{
    class TakeDamageMechanics : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent<int> unityDamageEvent;

        public void TakeDamage(int dmg)
        {
            unityDamageEvent?.Invoke(dmg);
        }
    }
}
