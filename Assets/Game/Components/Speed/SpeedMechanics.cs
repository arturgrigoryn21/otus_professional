﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Game.Components.Speed
{
    class SpeedMechanics : MonoBehaviour
    {
        [SerializeField]
        private float speed;
        public float Speed => speed;
        public event Action<float> OnSpeedChanged;
        public void SetSpeed(float speed)
        {
            this.speed = speed;
            OnSpeedChanged?.Invoke(speed);
        }
    }
}
