﻿using System;
using UnityEngine;

namespace Assets.Game.Components.Speed
{
    class SpeedComponent : MonoBehaviour, ISpeedComponent
    {
        public float Speed => speedMechanics.Speed;

        public event Action<float> OnSpeedChange
        {
            add { speedMechanics.OnSpeedChanged += value; }
            remove { speedMechanics.OnSpeedChanged -= value; }
        }

        [SerializeField]
        private SpeedMechanics speedMechanics;
        public void SetSpeed(float speed)
        {
            speedMechanics.SetSpeed(speed);
        }
    }
}
