﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Game.Components.Speed
{
    internal interface ISpeedComponent
    {
        event Action<float> OnSpeedChange;
        float Speed { get; }
        void SetSpeed(float speed);
    }
}
