﻿namespace Assets.Game.Scripts
{
    interface ITagComponent
    {
        public string Tag { get; }
    }
}
