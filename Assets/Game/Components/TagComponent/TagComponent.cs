﻿using UnityEngine;

namespace Assets.Game.Scripts
{
    class TagComponent : MonoBehaviour, ITagComponent
    {
        [SerializeField]
        private TagMechanics tagMechanics;
        public string Tag => tagMechanics.Tag;
    }
}
