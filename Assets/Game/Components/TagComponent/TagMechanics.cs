﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Assets.Game.Scripts
{
    class TagMechanics : MonoBehaviour
    {
        public string Tag { get; private set; }
        private void Start()
        {
            Tag = gameObject.tag;
        }
    }
}
