﻿using UnityEngine;


namespace Player
{
    public class PlayerStateAnimation : State
    {
        [SerializeField]
        private Animator animator;

        public PlayerState targetState;
        public override void EnterState()
        {
            animator.SetInteger("state", (int)targetState);
        }

        public override void ExitState()
        {
            animator.SetInteger("state", (int)PlayerState.Idle);
        }
    }

    
}


