﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace QUESTS
{
    public class QuestService : MonoBehaviour
    {
        [SerializeField]
        private MonoEntity manager;

        public MonoEntity Manager => manager;
    }
}
