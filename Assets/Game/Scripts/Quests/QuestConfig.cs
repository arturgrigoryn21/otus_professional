using UnityEngine;

namespace QUESTS
{
    public abstract class QuestConfig : ScriptableObject
    {
        [SerializeField]
        public string id;

        [SerializeField]
        public int moneyReward;

        public abstract IQuest InstatiateQuest();
    }
}