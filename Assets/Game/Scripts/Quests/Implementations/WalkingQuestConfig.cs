using UnityEngine;

namespace QUESTS
{
    [CreateAssetMenu(
        fileName = "WalkingQuestConfig",
        menuName = "QUESTS/WalkingQuestConfig"
    )]
    public class WalkingQuestConfig : QuestConfig
    {
        [SerializeField]
        public int requiredDistance;

        public override IQuest InstatiateQuest()
        {
            return new WalkingQuest(this);
        }
    }
}