using System;
using Assets.Scripts;
using Entities;
using GameSystems;
using Interfeces;
using UnityEngine;

namespace QUESTS
{
    public sealed class WalkingQuest : Quest, IGameInitElement
    {
        private readonly WalkingQuestConfig config;
        private float currentDistance;
        private IInfoComponent infoComponent;

        public override float Progress
        {
            get { return (float)this.currentDistance / this.config.requiredDistance; }
        }

        public override event Action<IQuest, float> OnProgressChanged;

        public WalkingQuest(WalkingQuestConfig config) : base(config)
        {
            this.config = config;
        }
        protected override void OnEnd()
        {
            infoComponent.OnDistanceChanged -= ChangeCurrentDinstance;
            currentDistance = 0;
        }

        protected override void OnStart()
        {
            infoComponent.OnDistanceChanged += ChangeCurrentDinstance;
        }

        private void ChangeCurrentDinstance(float distance)
        {
            currentDistance = Mathf.Min(distance, config.requiredDistance);
            OnProgressChanged?.Invoke(this, Progress);
            //Debug.Log(Progress);
        }

        public void OnInitGame(IGameSystem gameSystem)
        {
            infoComponent = gameSystem.GetService<PlayerService>().Player.Element<IInfoComponent>();
        }

        
    }
}