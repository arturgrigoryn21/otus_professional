﻿using Assets.Game.Scripts;
using Entities;
using GameSystems;
using Interfeces;
using UnityEngine;

namespace QUESTS
{
    public class QuestController : MonoBehaviour,
        IGameInitElement, IGameStartElement, IGameFinishElement
    {
        private QuestManager questManager;
        private ITriggerComponent zonaTrigger;
        public void OnFinishGame()
        {
            zonaTrigger.OnTriggerEnter -= StartQuest;
        }

        public void OnInitGame(IGameSystem gameSystem)
        {
            questManager = gameSystem.GetService<QuestService>().Manager.Element<QuestManager>();
            zonaTrigger = gameSystem.GetService<QuestZonaService>().QuestZona.Element<ITriggerComponent>();

            gameSystem.AddElementDuringProccess(questManager); ///////////////////
        }

        public void OnStartGame()
        {
            zonaTrigger.OnTriggerEnter += StartQuest;
        }

        private void StartQuest(Collider col)
        {
            if (IsItPlayer(col, out _))
            {
                if(questManager.CanTakeQuest())
                    questManager.TakeQuest();
                else if(questManager.CanReceiveReward())
                    questManager.ReceiveReward();
            }
        }

        private bool IsItPlayer(Collider collider, out IEntity entity)
        {
            if (collider.TryGetComponent(out entity) && entity.TryElement(out ITagComponent tag) && tag.Tag == "Player")
            {
                return true;
            }
            return false;
        }
    }
}