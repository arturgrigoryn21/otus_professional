using GameSystems;
using UnityEngine;

namespace QUESTS
{
    public sealed class QuestFactory : MonoBehaviour
    {
        public IGameSystem GameSystem { private get; set; }

        public IQuest CreateQuest(QuestConfig config)
        {
            var quest = config.InstatiateQuest();
            if (quest is IGameElement)
            {
                this.GameSystem.AddElementDuringProccess(quest);
            }

            return quest;
        }

        public void DisposeQuest(IQuest quest)
        {
            if (quest is IGameElement)
            {
                this.GameSystem.RemoveElement((IGameElement)quest);
            }
        }
    }
}