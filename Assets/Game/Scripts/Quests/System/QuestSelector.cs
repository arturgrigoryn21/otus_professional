using UnityEngine;

namespace QUESTS
{
    public sealed class QuestSelector : MonoBehaviour
    {
        [SerializeField]
        private QuestConfig[] configs;

        public QuestConfig SelectQuest()
        {
            var index = Random.Range(0, this.configs.Length);
            return this.configs[index];
        }
    }
}