using Assets.Game.Scripts.Common;
using GameSystems;
using System;
using UnityEngine;

namespace QUESTS
{

    public sealed class QuestManager : MonoBehaviour, IGameInitElement
    {
        public event Action<IQuest> OnTakeQuest;
        public event Action<IQuest> OnFinishQuest;
        public IQuest CurrentQuest { get; private set; }

        [SerializeField]
        private QuestSelector selector;

        [SerializeField]
        private QuestFactory factory;

        //private IMoneyBank moneyBank;

        public bool CanTakeQuest()
        {
            return this.CurrentQuest == null;
        }

        public void TakeQuest()
        {
            if (this.CurrentQuest != null)
            {
                throw new Exception("Quest is already exists!");
            }

            var questConfig = this.selector.SelectQuest();
            var quest = this.factory.CreateQuest(questConfig);
            this.CurrentQuest = quest;
            OnTakeQuest?.Invoke(quest);
            quest.Start();
        }

        public bool CanReceiveReward()
        {
            return this.CurrentQuest != null && this.CurrentQuest.IsCompleted;
        }

        public void ReceiveReward()
        {
            var quest = this.CurrentQuest;
            if (quest == null)
            {
                throw new Exception("Quest is null!");
            }

            if (!quest.IsCompleted)
            {
                throw new Exception("Quest is not completed!");
            }

            //this.moneyBank.EarnMoney(quest.MoneyReward);
            this.CurrentQuest = null;
            OnFinishQuest?.Invoke(quest);
            this.factory.DisposeQuest(quest);
        }


        public void OnInitGame(IGameSystem gameSystem)
        {
            factory.GameSystem = gameSystem;
        }
    }
}