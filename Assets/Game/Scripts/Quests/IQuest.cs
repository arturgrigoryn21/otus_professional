using System;

namespace QUESTS
{
    public interface IQuest
    {
        event Action<IQuest> OnStarted;

        event Action<IQuest, float> OnProgressChanged;

        event Action<IQuest> OnCompleted;

        event Action<IQuest> OnStopped;
        
        string Id { get; }
        
        float Progress { get; } //0..1
        
        bool IsCompleted { get; }
        
        int MoneyReward { get; } //Big Integer

        void Start(); //Listen model

        void Stop(); //Unlisten model
    }
}