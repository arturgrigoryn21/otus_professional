﻿using Entities;
using UnityEngine;

namespace QUESTS
{
    public class QuestZonaService : MonoBehaviour
    {
        [SerializeField]
        private MonoEntity questZona;

        public MonoEntity QuestZona => questZona;
    }
}
