using System;


namespace QUESTS
{
    public abstract class Quest : IQuest
    {
        public event Action<IQuest> OnStarted;

        public abstract event Action<IQuest, float> OnProgressChanged; //!

        public event Action<IQuest> OnCompleted;

        public event Action<IQuest> OnStopped;

        public string Id
        {
            get { return this.config.id; }
        }

        public bool IsCompleted { get; private set; }

        public abstract float Progress { get; } //!

        public int MoneyReward
        {
            get { return this.config.moneyReward; }
        }

        private readonly QuestConfig config;

        private bool isStarted;

        public Quest(QuestConfig config)
        {
            this.config = config;
        }

        public void Start()
        {
            this.IsCompleted = false;
            this.isStarted = true;
            this.OnStarted?.Invoke(this);

            if (this.Progress >= 1.0f)
            {
                //Complete quest
                this.Complete();
            }
            else
            {
                //Process:
                this.OnStart();
            }
        }

        public void Stop()
        {
            if (!this.isStarted)
            {
                return;
            }

            this.isStarted = false;
            this.OnEnd();
            this.OnStopped?.Invoke(this);
        }

        private void Complete()
        {
            if (this.IsCompleted)
            {
                return;
            }

            this.isStarted = false;
            this.IsCompleted = true;
            this.OnEnd();
            this.OnCompleted?.Invoke(this);
        }

        protected abstract void OnStart();

        protected abstract void OnEnd();

        protected void TryComplete()
        {
            if (this.Progress >= 1)
            {
                this.Complete();
            }
        }
    }
}