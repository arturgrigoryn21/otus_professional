﻿
using Entities;

internal interface IPlayerBag
{
    int GetRecourcesCount(ResourceType resourceType);
    void ExtractResources(ResourceType resourceType, int putAmount);
    void PutResources(ResourceType targetResource, int collectAmount);
}