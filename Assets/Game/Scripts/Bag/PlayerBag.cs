﻿using Entities;
using System;
using System.Collections.Generic;
using UnityEngine;

class PlayerBag : MonoBehaviour, IPlayerBag
{
    private Dictionary<ResourceType, int> recources;

    private void Start()
    {
        recources = new Dictionary<ResourceType, int>()
        {
            {ResourceType.Wood, 0 },
            {ResourceType.Rock, 0 },
            {ResourceType.Axe, 0 }
        };
    }

    public void ExtractResources(ResourceType resourceType, int putAmount)
    {
        recources[resourceType] = Math.Max(recources[resourceType] - putAmount, 0);
    }

    public int GetRecourcesCount(ResourceType resourceType)
    {
        return recources.ContainsKey(resourceType) ? recources[resourceType] : 0;
    }

    public void PutResources(ResourceType targetResource, int collectAmount)
    {
        recources[targetResource] += collectAmount;
    }
}
