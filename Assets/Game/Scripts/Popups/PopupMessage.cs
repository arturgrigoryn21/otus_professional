using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class PopupMessage : MonoPopup
{
    [SerializeField]
    private Text text;

    public override void Show(IPopupArgs popupArgs)
    {
        base.Show(popupArgs);
        var args = (PopupMessageArgs)popupArgs;
        text.text = args.Text;
    }

    public override void Hide()
    {
        base.Hide();
        text.text = "";
    }

    public struct PopupMessageArgs : IPopupArgs
    {
        public string Text { get; }

        public PopupMessageArgs(string text)
        {
            Text = text;
        }
    }
}
