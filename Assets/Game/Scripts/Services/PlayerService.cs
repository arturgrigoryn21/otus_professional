using Entities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class PlayerService : MonoBehaviour
    {
        [SerializeField]
        private MonoEntity player;

        public MonoEntity Player => player;
    }
}
