﻿using Interfeces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    class PlayerInfoService : MonoBehaviour
    {
        [SerializeField]
        private IInfoComponent infoComponent;

        public event Action<float> OnDistanceChanged;

        private void Start()
        {
            infoComponent.OnDistanceChanged += OnDistanceChange;
        }
        public void OnDistanceChange(float distance)
        {
            OnDistanceChanged?.Invoke(distance);
            
        }
    }
}
