using Entities;
using Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamageController : MonoBehaviour
{
    [SerializeField]
    private MonoEntity player;
    private ITakeDamageComponent takeDmgComponent;

    private void Start()
    {
        this.takeDmgComponent = this.player.Element<ITakeDamageComponent>();
    }
}
