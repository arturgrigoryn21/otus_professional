using Entities;
using Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Interfaces;
using Assets.Scripts;
using GameSystems;

public class MoveContoller : MonoBehaviour,
    IGameStartElement, IGameFinishElement, IGameInitElement
{
    private MonoEntity player;
    private IMoveComponent moveComponent;
    private InputKeyManager inputManager;


    public void OnStartGame()
    {
        inputManager.OnInput_Move += Move;
    }

    public void OnFinishGame()
    {
        inputManager.OnInput_Move -= Move;
    }


    private void Move(Vector3 direction)
    {
        moveComponent.MoveByKey(direction);
    }

    public void OnInitGame(IGameSystem gameSystem)
    {
        inputManager = gameSystem.GetService<InputKeyManager>();
        player = gameSystem.GetService<PlayerService>().Player;
        moveComponent = player.Element<IMoveComponent>();
    }
}
