﻿using GameSystems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    class InputManagerController: MonoBehaviour, 
        IGameStartElement, IGameFinishElement, IGameInitElement
    {
        private InputKeyManager inputManager;

        public void OnFinishGame()
        {
            inputManager.enabled = false;
        }

        public void OnInitGame(IGameSystem gameSystem)
        {
            inputManager = gameSystem.GetService<InputKeyManager>();
        }

        public void OnStartGame()
        {
            inputManager.enabled = true;
        }
    }
}
