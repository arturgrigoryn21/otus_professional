﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    class InputKeyManager : MonoBehaviour
    {
        public event Action<Vector3> OnInput_Move;
        public event Action<Vector3> OnInput_Rotation;

        private void Awake()
        {
            this.enabled = false;
        }
        private void Update()
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                OnInput_Move?.Invoke(Vector3.forward);
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                OnInput_Move?.Invoke(Vector3.back);
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                OnInput_Move?.Invoke(Vector3.left);
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                OnInput_Move?.Invoke(Vector3.right);
            }
            if(Input.GetKey(KeyCode.Q))
            {
                OnInput_Rotation?.Invoke(Vector3.up);
            }
            if (Input.GetKey(KeyCode.E))
            {
                OnInput_Rotation?.Invoke(Vector3.down);
            }
        }
    }
}
