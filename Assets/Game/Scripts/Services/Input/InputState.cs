﻿
using UnityEngine;

namespace Assets.Scripts
{
    class InputState : State
    {
        [SerializeField]
        private MonoBehaviour inputManager;

        public override void EnterState()
        {
            inputManager.enabled = true;
        }

        public override void ExitState()
        {
            inputManager.enabled = false;
        }
    }
}
