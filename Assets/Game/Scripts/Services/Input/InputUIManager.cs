﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    class InputUIManager : MonoBehaviour
    {
        public event Action OnSwitchInput;

        public void SwitchInputHandler()
        {
            OnSwitchInput?.Invoke();
        }
    }
}
