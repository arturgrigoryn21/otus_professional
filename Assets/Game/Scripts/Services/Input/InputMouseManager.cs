﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    class InputMouseManager : MonoBehaviour
    {
        public event Action<Vector3> OnInput_Move;

        private void Update()
        {
            if(Input.GetMouseButton(0))
            {
                var position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                OnInput_Move?.Invoke(position);
            }
        }
    }
}
