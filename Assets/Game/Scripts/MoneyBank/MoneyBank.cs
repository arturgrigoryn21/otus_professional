﻿using System;
using UnityEngine;

namespace Assets.Game.Scripts.Upgrades
{
    internal class MoneyBank : MonoBehaviour
    {
        internal Action<int, int> OnMoneyChanged;

        [SerializeField]
        private int money;
        public int Money => this.money;

        [SerializeField]
        private int change;
        public void SpendMoney(int nextPrice)
        {
            var oldValue = this.money;
            this.money = Mathf.Max(money - nextPrice, 0);
            OnMoneyChanged?.Invoke(oldValue, this.money);
        }

        [ContextMenu("ChangeMoney")]
        public void ChangeMoney()
        {
            var oldValue = this.money;
            this.money += change;
            OnMoneyChanged?.Invoke(oldValue, this.money);
        }
    }
}