﻿using UnityEditor;
using UnityEngine;

namespace Assets.Game.Scripts.Upgrades
{
    [CustomEditor(typeof(MoneyBank))]
    public class MoneyEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            GUILayout.Label("Label text");
            DrawDefaultInspector();
            MoneyBank bank = (MoneyBank)target;
            if (GUILayout.Button("Change"))
            {
                bank.ChangeMoney();
            }
        }
    }
}