using UnityEngine;
using UnityEngine.UI;


public sealed class ProgressBar : MonoBehaviour
{
    [SerializeField]
    private Image progress;

    [SerializeField]
    private GameObject root;

    public void SetVisible(bool isVisible)
    {
        this.root.SetActive(isVisible);
    }

    public void SetProgress(float progress)
    {
        this.progress.fillAmount = Mathf.Clamp01(progress);
    }
}
