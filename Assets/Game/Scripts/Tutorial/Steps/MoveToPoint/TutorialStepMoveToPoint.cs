﻿using Assets.Game.Scripts.Common;
using Assets.Scripts;
using Entities;
using GameSystems;
using Interfaces;
using System;
using UnityEngine;

namespace Assets.Game.Scripts.Tutorial
{

    public class TutorialStepMoveToPoint : TutorialStep, IGameInitElement
    {
        [SerializeField]
        private Transform aimPosition;

        [SerializeField]
        private TutorialArrow aim;

        [SerializeField]
        private float minDistances = 5;

        public override event Action OnFinishedStep;
        private IMoveComponent moveComponent;
        private ITransformComponent transformComponent;

        private PopupManager popupManager;

        void IGameInitElement.OnInitGame(IGameSystem gameSystem)
        {
            moveComponent = gameSystem.GetService<PlayerService>().Player.Element<IMoveComponent>();
            transformComponent = gameSystem.GetService<PlayerService>().Player.Element<TransformComponent>();
            popupManager = gameSystem.GetService<PopupManager>();
        }

        public override void Begin()
        {
            popupManager.Show(PopupName.MessagePopup, new PopupMessage.PopupMessageArgs(config.Text));
            aim.Active();
            aim.MoveTo(aimPosition.position);
            moveComponent.OnMovingEvent += CheckExecution;
        }


        private void CheckExecution()
        {
            if ((transformComponent.Position - aim.transform.position).magnitude < minDistances)
            {
                Finish();
            }
        }
        private void Finish()
        {
            popupManager.Hide(PopupName.MessagePopup);
            moveComponent.OnMovingEvent -= CheckExecution;
            aim.Deactivate();
            OnFinishedStep?.Invoke();
        }
    }
}
