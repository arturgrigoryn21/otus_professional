﻿using UnityEngine;

namespace Assets.Game.Scripts.Upgrades
{
    public class TutorialUpgradesPresenterManager : UpgradesPresenterManager
    {
        [SerializeField]
        private UpgradeView damageUpgradeView;
        public override void Show()
        {
            var upgrades = this.manager.GetAllUpgrades();
            foreach (var upgrade in upgrades)
            {
                //if this dmg upgrade use alreade existed view
                UpgradeView view = upgrade is PlayerDamageUpgrade? damageUpgradeView : Instantiate(this.viewPrefab, this.viewsContainer);
                this.views.Add(view);

                UpgradePresenter presenter = new UpgradePresenter(upgrade, view);
                this.gameSystem.AddElementDuringProccess(presenter);
                this.presenters.Add(presenter);

                presenter.Show();
            }
        }
    }
}