﻿using Assets.Game.Scripts.Upgrades;
using GameSystems;
using System;

namespace Assets.Game.Scripts.Tutorial
{

    public class TutorialStepUpgrade : TutorialStep, 
        IGameInitElement, IGameStartElement, IGameFinishElement
    {
        public override event Action OnFinishedStep;

        private PopupManager popupManager;
        private MoneyBank moneyBank;
        public override void Begin()
        {
            popupManager.Show(PopupName.MessagePopup, new PopupMessage.PopupMessageArgs(config.Text));
            popupManager.Show(PopupName.UpgradePopupTutorial1);
        }

        void IGameInitElement.OnInitGame(IGameSystem gameSystem)
        {
            popupManager = gameSystem.GetService<PopupManager>();
            moneyBank = gameSystem.GetService<MoneyBank>();
        }

        void IGameStartElement.OnStartGame()
        {
            moneyBank.OnMoneyChanged += CheckMoneyCount;
        }
        void IGameFinishElement.OnFinishGame()
        {
            moneyBank.OnMoneyChanged -= CheckMoneyCount;
        }
        private void CheckMoneyCount(int before, int now)
        {
            if (now <= 0)
            {
                Finish();
            }
        }

        private void Finish()
        {
            popupManager.Hide(PopupName.UpgradePopupTutorial1);
            popupManager.Hide(PopupName.MessagePopup);
            OnFinishedStep?.Invoke();
        }

        
    }
}
