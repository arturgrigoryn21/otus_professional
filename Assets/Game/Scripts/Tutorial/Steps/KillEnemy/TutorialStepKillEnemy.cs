﻿using Entities;
using GameSystems;
using Interfaces;
using System;
using UnityEngine;

namespace Assets.Game.Scripts.Tutorial
{
    public class TutorialStepKillEnemy : TutorialStep, IGameInitElement
    {
        [SerializeField]
        private MonoEntity enemy;
        private PopupManager popupManager;

        private IDieComponent dieComponent;

        public override event Action OnFinishedStep;

        public override void Begin()
        {
            popupManager.Show(PopupName.MessagePopup, new PopupMessage.PopupMessageArgs(config.Text));
            dieComponent.Died += Finish;
        }

        private void Finish()
        {
            popupManager.Hide(PopupName.MessagePopup);
            OnFinishedStep?.Invoke();
        }

        void IGameInitElement.OnInitGame(IGameSystem gameSystem)
        {
            popupManager = gameSystem.GetService<PopupManager>();
            dieComponent = enemy.Element<IDieComponent>();
        }
    }
}
