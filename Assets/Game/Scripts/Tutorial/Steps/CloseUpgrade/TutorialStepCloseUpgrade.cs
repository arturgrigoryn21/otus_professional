﻿using GameSystems;
using System;

namespace Assets.Game.Scripts.Tutorial
{
    public class TutorialStepCloseUpgrade : TutorialStep,
        IGameInitElement
    {
        private PopupManager popupManager;

        public override event Action OnFinishedStep;

        public override void Begin()
        {
            popupManager.Show(PopupName.UpgradePopupTutorial2);
            popupManager.Show(PopupName.MessagePopup, new PopupMessage.PopupMessageArgs(config.Text));
            popupManager.OnPopupHidden += CheckPopupClosed;
        }

        private void CheckPopupClosed(PopupName name)
        {
            if (name == PopupName.UpgradePopupTutorial2)
                Finish();
        }

        private void Finish()
        {
            popupManager.OnPopupHidden -= CheckPopupClosed;
            popupManager.Hide(PopupName.MessagePopup);
            OnFinishedStep?.Invoke();
        }

        void IGameInitElement.OnInitGame(IGameSystem gameSystem)
        {
            popupManager = gameSystem.GetService<PopupManager>();
        }
    }
}
