﻿using GameSystems;
using UnityEngine;

namespace Assets.Game.Scripts.Tutorial
{
    public abstract class TutorialObserver : MonoBehaviour, 
        IGameStartElement, IGameFinishElement
    {
        [SerializeField]
        protected TutorialStepType expectedStep;

        protected virtual void CheckStep(TutorialStepType step) { }

        public virtual void OnFinishGame()
        {
            TutorialManager.Instance.OnMoveStep -= CheckStep;
        }

        public virtual void OnStartGame()
        {
            TutorialManager.Instance.OnMoveStep += CheckStep;
        }
    }
}
