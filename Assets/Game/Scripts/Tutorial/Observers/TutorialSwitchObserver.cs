﻿using Entities;
using GameSystems;
using UnityEngine;

namespace Assets.Game.Scripts.Tutorial
{
    public class TutorialSwitchObserver : TutorialObserver,
        IGameStartElement, IGameFinishElement
    {
        [SerializeField]
        private MonoEntity controlEntity;

        private ISwitchComponent switchComponent;

        public override void OnStartGame()
        {
            base.OnStartGame();
            switchComponent = controlEntity.Element<ISwitchComponent>();           
        }

        protected override void CheckStep(TutorialStepType step)
        {
            if(step >= expectedStep)
            {
                switchComponent.SwitchOn();
            }
            else
            {
                switchComponent.SwitchOff();
            }
        }
    }
}
