﻿using GameSystems;
using UnityEngine;

namespace Assets.Game.Scripts.Tutorial
{
    public class TutorialActivateObserver : TutorialObserver, 
        IGameStartElement, IGameFinishElement
    {

        [SerializeField]
        private GameObject controlObject;

        protected override void CheckStep(TutorialStepType step)
        {
            if (step >= expectedStep)
                controlObject.SetActive(true);
            else
                controlObject.SetActive(false);
        }       
    }
}
