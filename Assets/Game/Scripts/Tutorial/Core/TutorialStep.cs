﻿using System;
using UnityEngine;

namespace Assets.Game.Scripts.Tutorial
{
    public abstract class TutorialStep : MonoBehaviour
    {
        public abstract event Action OnFinishedStep;
        public abstract void Begin();
        public TutorialStepType StepType => stepType;

        [SerializeField]
        private TutorialStepType stepType;

        [SerializeField]
        protected TutorialStepConfig config;
    }
}
