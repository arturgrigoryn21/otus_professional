﻿using UnityEngine;

namespace Assets.Game.Scripts.Tutorial
{
    [CreateAssetMenu(
        fileName = "TutorialStepConfig",
        menuName = "GameEngine/TutorialStepConfig"
    )]
    public class TutorialStepConfig : ScriptableObject
    {
        [SerializeField]
        private string text;

        [SerializeField]
        private Sprite sprite;

        public string Text  => text;

        public Sprite Sprite => sprite; 
    }
}
