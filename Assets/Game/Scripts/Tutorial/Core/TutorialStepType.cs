﻿namespace Assets.Game.Scripts.Tutorial
{
    public enum TutorialStepType
    {
        MoveToPoint,
        MoveToUpgrade,
        Upgrade,
        CloseUpgrade,
        KillEnemy
    }
}
