using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialArrow : MonoBehaviour
{
    [SerializeField]
    private Transform _transform;


    public void Active()
    {
        gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }

    public void MoveTo(Vector3 position)
    {
        _transform.position = position;
    }

    public void Rotate(Quaternion quaternion)
    {
        _transform.rotation = quaternion;
    }
}
