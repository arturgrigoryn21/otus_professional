﻿using GameSystems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Game.Scripts.Tutorial
{
    internal class TutorialManager : MonoBehaviour
    {
        [SerializeField]
        private MonoGameSystem gameSystem;

        [SerializeField]
        private List<TutorialStep> tutorialSteps = new List<TutorialStep>();

        private TutorialObserver[] tutorialObservers;

        [SerializeField]
        private TutorialStepType startStepIndex;

        private int currentStepIndex = 0;
        public int CurrentStepIndex => currentStepIndex;
        public event Action<TutorialStepType> OnMoveStep;

        [SerializeField]
        private bool autoLaunch;

        public static TutorialManager Instance { get; private set; }

        private void Awake()
        {
            if (Instance != null)
                throw new Exception("Singleton");
            Instance = this;

            tutorialObservers = Resources.FindObjectsOfTypeAll(typeof(TutorialObserver)) as TutorialObserver[];
        }

        private void Start()
        {
            foreach (var step in tutorialSteps)
                gameSystem.AddElementDuringProccess(step);

            foreach (var observer in tutorialObservers)
                gameSystem.AddElementDuringProccess(observer);

            currentStepIndex = (int)startStepIndex;
            if (autoLaunch)
                MoveStep();
        }


        [ContextMenu("MoveStep")]
        public void MoveStep()
        {
            if (currentStepIndex >= tutorialSteps.Count)
                return;
            var step = tutorialSteps[currentStepIndex];
            Debug.Log(step.StepType);
            OnMoveStep?.Invoke(step.StepType);
            step.OnFinishedStep += MoveStep;
            step.Begin();

            currentStepIndex++;
        }


        [ContextMenu("BeginFromStart")]
        public void BeginFromStart()
        {
            currentStepIndex = 0;
            MoveStep();
        }
    }
}
