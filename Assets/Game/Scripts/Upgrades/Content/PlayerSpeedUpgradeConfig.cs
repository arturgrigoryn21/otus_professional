﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Game.Scripts.Upgrades
{
    [CreateAssetMenu(
        fileName = "SpeedUpgradeConfig",
        menuName = "Upgrades/SpeedUpgradeConfig"
    )]
    public class PlayerSpeedUpgradeConfig : PlayerUpgradeConfig
    {
        private Dictionary<int, float> speedLevel;

        public PlayerSpeedUpgradeConfig()
        {
            this.speedLevel = new Dictionary<int, float>
            {
                {2,  6},
                {3,  8},
                {4,  10},
                {5,  12}
            };
        }
        public float GetSpeed(int level)
        {
            return speedLevel[level];
        }
        public override IPlayerUpgrade InstantiateUpgrade()
        {
            return new PlayerSpeedUpgrade(this);
        }
    }
}
