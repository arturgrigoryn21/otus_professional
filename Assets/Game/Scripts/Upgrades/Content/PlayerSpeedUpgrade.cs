﻿using Assets.Game.Components.Speed;
using Assets.Scripts;
using GameSystems;
using System;

namespace Assets.Game.Scripts.Upgrades
{
    public class PlayerSpeedUpgrade : PlayerUpgrade, IGameInitElement
    {
        private ISpeedComponent speedComponent;
        private new PlayerSpeedUpgradeConfig config;
        public PlayerSpeedUpgrade(PlayerSpeedUpgradeConfig config) : base(config) 
        {
            this.config = config;
        }
        
        protected override void OnLevelUp(int level)
        {
            var newSpeed = this.config.GetSpeed(level);
            this.speedComponent.SetSpeed(newSpeed);
        }

        public void OnInitGame(IGameSystem gameSystem)
        {
            this.speedComponent = gameSystem.GetService<PlayerService>().Player.Element<ISpeedComponent>();
        }
    }
}
