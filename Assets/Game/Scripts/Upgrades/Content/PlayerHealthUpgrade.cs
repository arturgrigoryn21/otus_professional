﻿using Assets.Scripts;
using GameSystems;
using Interfaces;

namespace Assets.Game.Scripts.Upgrades
{
    public class PlayerHealthUpgrade : PlayerUpgrade, IGameInitElement
    {
        private new PlayerHealthUpgradeConfig config;
        private IHealthComponent healthComponent;
        public PlayerHealthUpgrade(PlayerHealthUpgradeConfig config) : base(config)
        {
            this.config = config;
        }

        public void OnInitGame(IGameSystem gameSystem)
        {
            this.healthComponent = gameSystem.GetService<PlayerService>().Player.Element<IHealthComponent>();
        }

        protected override void OnLevelUp(int level)
        {
            var health = this.config.GetHealth(level);
            this.healthComponent.SetHealth(health);
        }
    }
}
