﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Game.Scripts.Upgrades
{
    [CreateAssetMenu(
        fileName = "HealthUpgradeConfig",
        menuName = "Upgrades/HealthUpgradeConfig"
    )]
    public class PlayerHealthUpgradeConfig : PlayerUpgradeConfig
    {
        private Dictionary<int, int> healthLevel;

        public PlayerHealthUpgradeConfig()
        {
            healthLevel = new Dictionary<int, int>
            {
                {2, 50 },
                {3, 75 },
                {4, 100 },
                {5, 125 }
            };
        }

        public int GetHealth(int level)
        {
            return healthLevel[level];
        }
        public override IPlayerUpgrade InstantiateUpgrade()
        {
            return new PlayerHealthUpgrade(this);
        }
    }
}
