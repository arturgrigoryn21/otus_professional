﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Game.Scripts.Upgrades
{
    [CreateAssetMenu(
        fileName = "DamageUpgradeConfig",
        menuName = "Upgrades/DamageUpgradeConfig"
    )]
    public class PlayerDamageUpgradeConfig : PlayerUpgradeConfig
    {
        private Dictionary<int, int> damageLevel;

        public PlayerDamageUpgradeConfig()
        {
            damageLevel = new Dictionary<int, int>
            {
                {2, 5 },
                {3, 10 },
                {4, 15 },
                {5, 20 },
            };
        }

        public override IPlayerUpgrade InstantiateUpgrade()
        {
            return new PlayerDamageUpgrade(this);
        }

        internal int GetDamage(int level)
        {
            return damageLevel[level];
        }
    }
}