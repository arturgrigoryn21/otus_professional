﻿using Assets.Game.Components.Damage;
using Assets.Scripts;
using GameSystems;

namespace Assets.Game.Scripts.Upgrades
{
    public class PlayerDamageUpgrade : PlayerUpgrade, IGameInitElement
    {
        private new PlayerDamageUpgradeConfig config;
        private IDamageComponent damageComponent;
        public PlayerDamageUpgrade(PlayerDamageUpgradeConfig config) : base(config)
        {
            this.config = config;
        }

        public void OnInitGame(IGameSystem gameSystem)
        {
            this.damageComponent = gameSystem.GetService<PlayerService>().Player.Element<IDamageComponent>();
        }

        protected override void OnLevelUp(int level)
        {
            var newDamage = this.config.GetDamage(level);
            this.damageComponent.SetDamage(newDamage);
        }
    }
}
