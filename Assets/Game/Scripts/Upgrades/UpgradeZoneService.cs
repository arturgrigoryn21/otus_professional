﻿using Entities;
using UnityEngine;

namespace Assets.Game.Scripts.Upgrades
{
    public class UpgradeZoneService : MonoBehaviour
    {
        [SerializeField]
        private MonoEntity upgradeZona;

        public MonoEntity UpgradeZona => upgradeZona;
    }
}