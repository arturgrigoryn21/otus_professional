﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Assets.Game.Scripts.Upgrades
{
    [CustomEditor(typeof(UpdateDebug))]
    public class DebugEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            GUILayout.Label("Label text");
            DrawDefaultInspector();
            UpdateDebug debug = (UpdateDebug)target;
            if (GUILayout.Button("Instantiate"))
            {
                debug.Activate();
            }
        }
    }
}
