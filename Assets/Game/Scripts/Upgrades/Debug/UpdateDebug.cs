﻿using GameSystems;
using UnityEngine;

namespace Assets.Game.Scripts.Upgrades
{
    public class UpdateDebug: MonoBehaviour
    {
        [SerializeField]
        private PopupManager manager;

        public void Activate()
        {
            manager.Show(PopupName.UpgradePopup);
        }
    }
}
