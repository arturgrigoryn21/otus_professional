﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Game.Scripts.Upgrades
{
    public abstract class PlayerUpgradeConfig : ScriptableObject
    {
        protected const float SPACE_HEIGHT = 10.0f;

        [SerializeField]
        public string id;

        [Range(2, 99)]
        [SerializeField]
        public int maxLevel = 2;

        [Space(SPACE_HEIGHT)]
        [SerializeField]
        public List<LevelPrice> priceTable;

        [SerializeField]
        private string title;
        public string Title => title;

        [SerializeField]
        private Sprite icon;
        public Sprite Icon => icon;
        public int GetLevelPrice(int level)
        {
            return priceTable.First(x => x.level == level).price;
        }
        public abstract IPlayerUpgrade InstantiateUpgrade();
    }
}
