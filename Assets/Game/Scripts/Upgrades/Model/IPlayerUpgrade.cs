﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Game.Scripts.Upgrades
{
    public interface IPlayerUpgrade
    {
        string Id { get; }

        int Level { get; }

        int MaxLevel { get; }

        int NextPrice { get; }
        string Title { get;}
        Sprite Icon { get; }

        event Action<int> OnLevelUpEvent;

        void LevelUp();
        bool IsMaxLevel();
    }
}
