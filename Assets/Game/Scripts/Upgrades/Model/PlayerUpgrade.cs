﻿using GameSystems;
using System;
using UnityEngine;

namespace Assets.Game.Scripts.Upgrades
{
    public abstract class PlayerUpgrade : IPlayerUpgrade
    {
        public string Id
        {
            get { return this.config.id; }
        }

        public int Level { get; private set; }

        public int MaxLevel
        {
            get { return this.config.maxLevel; }
        }

        public int NextPrice
        {
            get
            {
                if (!IsMaxLevel())
                    return this.config.GetLevelPrice(Level + 1);
                return 0;
            }
        }

        public string Title => config.Title;

        public Sprite Icon => config.Icon;

        protected readonly PlayerUpgradeConfig config;

        public PlayerUpgrade(PlayerUpgradeConfig config)
        {
            this.config = config;
            this.Level = 1;
        }

        public event Action<int> OnLevelUpEvent;


        public void LevelUp()
        {
            if (this.Level >= this.MaxLevel)
            {
                throw new Exception("Max level is reached!");
            }

            var nextLevel = this.Level + 1;
            this.Level = nextLevel;
            this.OnLevelUp(nextLevel);
            OnLevelUpEvent?.Invoke(nextLevel);
        }

        protected abstract void OnLevelUp(int level);

        public bool IsMaxLevel()
        {
            return Level == MaxLevel;
        }
    }
}
