﻿using System;

namespace Assets.Game.Scripts.Upgrades
{
    [Serializable]
    public class LevelPrice
    {
        public int price;
        public int level;
    }
}
