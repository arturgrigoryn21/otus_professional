﻿using GameSystems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Game.Scripts.Upgrades
{
    public class PlayerUpgradesManager : MonoBehaviour, IGameInitElement
    {
        [SerializeField]
        private PlayerUpgradeConfig[] configs;

        private Dictionary<string, IPlayerUpgrade> upgrades;

        private MoneyBank moneyBank;

        public IPlayerUpgrade GetUpgrade(string id)
        {
            return this.upgrades[id];
        }

        public IPlayerUpgrade[] GetAllUpgrades()
        {
            return this.upgrades.Values.ToArray();
        }

        public bool CheckUpgrade(IPlayerUpgrade upgrade)
        {
            if (upgrade.Level >= upgrade.MaxLevel)
            {
                return false;
            }

            return this.moneyBank.Money >= upgrade.NextPrice;
        }

        public void LevelUp(string idUpgrade)
        {
            var upgrade = this.GetUpgrade(idUpgrade);
            if (!this.CheckUpgrade(upgrade))
            {
                Debug.Log("Can not upgrade!");
                return;
            }

            this.moneyBank.SpendMoney(upgrade.NextPrice);
            upgrade.LevelUp();
        }

        public void LevelUp(IPlayerUpgrade upgrade)
        {
            if (!this.CheckUpgrade(upgrade))
            {
                Debug.Log("Can not upgrade!");
                return;
            }

            this.moneyBank.SpendMoney(upgrade.NextPrice);
            upgrade.LevelUp();
        }

        private void Awake()
        {
            this.upgrades = new Dictionary<string, IPlayerUpgrade>();
            foreach (var config in this.configs)
            {
                var upgrade = config.InstantiateUpgrade();
                this.upgrades.Add(upgrade.Id, upgrade);
            }
        }

        public void OnInitGame(IGameSystem gameSystem)
        {
            this.moneyBank = gameSystem.GetService<MoneyBank>();
            foreach (var upgrade in this.upgrades.Values)
            {
                gameSystem.AddElementDuringProccess(upgrade);
            }
        }
    }
}
