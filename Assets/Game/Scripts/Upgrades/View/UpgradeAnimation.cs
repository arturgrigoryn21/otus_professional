﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Assets.Game.Scripts.Upgrades
{
    public class UpgradeAnimation
    {
        public void ButtonAnima(Button button)
        {
            var sequnces = DOTween.Sequence();
            sequnces.Append(button.transform.DOScale(new Vector3(1.1f, 1.1f, 1), 0.1f));
            sequnces.Append(button.transform.DOScale(new Vector3(1f, 1f, 1), 0.1f));
        }
    }
}