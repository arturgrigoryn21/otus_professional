﻿using Entities;
using GameSystems;
using Interfeces;
using UnityEngine;

namespace Assets.Game.Scripts.Upgrades
{
    public class UpgradeController : MonoBehaviour, 
        IGameInitElement, IGameStartElement, IGameFinishElement
    {
        private ITriggerComponent upgradeZone;

        private PopupManager popupManager;
        public void OnInitGame(IGameSystem gameSystem)
        {
            this.upgradeZone = gameSystem.GetService<UpgradeZoneService>().UpgradeZona.Element<ITriggerComponent>();
            this.popupManager = gameSystem.GetService<PopupManager>();
        }

        public void OnStartGame()
        {
            this.upgradeZone.OnTriggerEnter += SwitchOnPopup;
        }
        public void OnFinishGame()
        {
            this.upgradeZone.OnTriggerEnter -= SwitchOnPopup;
        }
        private void SwitchOnPopup(Collider col)
        {
            if (IsItPlayer(col, out _))
            {
                popupManager.Show(PopupName.UpgradePopup);
            }
        }


        private bool IsItPlayer(Collider collider, out IEntity entity)
        {
            if (collider.TryGetComponent(out entity) && entity.TryElement(out ITagComponent tag) && tag.Tag == "Player")
            {
                return true;
            }
            return false;
        }

        
    }
}