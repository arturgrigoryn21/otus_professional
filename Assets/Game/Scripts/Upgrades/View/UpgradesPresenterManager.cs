using System.Collections.Generic;
using Assets.Game.Scripts.Upgrades;
using GameSystems;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Game.Scripts.Upgrades
{
    public class UpgradesPresenterManager : MonoBehaviour, IGameInitElement
    {
        [SerializeField]
        protected UpgradeView viewPrefab;

        [SerializeField]
        protected Transform viewsContainer;

        [SerializeField]
        private CloseButton closeButton;

        protected PlayerUpgradesManager manager;

        protected IGameSystem gameSystem;
        protected PopupManager popupManager;
        protected readonly List<UpgradePresenter> presenters = new List<UpgradePresenter>();

        protected readonly List<UpgradeView> views = new List<UpgradeView>();


        private void OnEnable()
        {
            gameSystem = FindObjectOfType<MonoGameSystem>();
            gameSystem.AddElementDuringProccess(this);
            popupManager = gameSystem.GetService<PopupManager>();

            closeButton.AddListener(Hide);
   
            Show();
        }

        private void OnDisable()
        {
            Hide();
        }


        public virtual void Show()
        {
            var upgrades = this.manager.GetAllUpgrades();
            foreach (var upgrade in upgrades)
            {
                UpgradeView view = Instantiate(this.viewPrefab, this.viewsContainer);  
                this.views.Add(view);
                
                UpgradePresenter presenter = new UpgradePresenter(upgrade, view);
                this.gameSystem.AddElementDuringProccess(presenter);
                this.presenters.Add(presenter);
                
                presenter.Show();
            }
        }

        public void Hide()
        {
            foreach (var presenter in this.presenters)
            {
                presenter.Hide();
                this.gameSystem.RemoveElement(presenter);
            }
            
            this.presenters.Clear();

            foreach (var view in this.views)
            {
                Destroy(view.gameObject); 
            }
            
            this.views.Clear();

            popupManager.Hide(closeButton.PopupName);
        }

        public void OnInitGame(IGameSystem gameSystem)
        {
            this.manager = gameSystem.GetService<PlayerUpgradesManager>();
            this.gameSystem = gameSystem;
        }
    }
}