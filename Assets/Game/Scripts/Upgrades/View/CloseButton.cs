﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.Game.Scripts.Upgrades
{
    public class CloseButton : MonoBehaviour
    {
        [SerializeField]
        private Button button;

        [SerializeField]
        private PopupName popupName;

        public PopupName PopupName  => popupName; 

        public void AddListener(UnityAction action)
        {
            button.onClick.AddListener(action);
        }
    }
}