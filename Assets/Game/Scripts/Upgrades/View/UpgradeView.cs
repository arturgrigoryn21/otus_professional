using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Game.Scripts.Upgrades
{
    public sealed class UpgradeView : MonoBehaviour
    {
        private const string UPGRADE_COLOR_HEX = "309D1E";

        public UpgradeButton UpgradeButton
        {
            get { return this.upgradeButton; }
        }

        [SerializeField]
        private Image iconImage;

        [Space]
        [SerializeField]
        private TextMeshProUGUI titleText;

        [SerializeField]
        private TextMeshProUGUI valueText;

        [SerializeField]
        private TextMeshProUGUI levelText;

        [Space]
        [SerializeField]
        private UpgradeButton upgradeButton;

        private UpgradeAnimation upgradeAnimation;

        

        private void OnEnable()
        {
            this.upgradeAnimation = new UpgradeAnimation();
            this.upgradeButton.OnClickButton += upgradeAnimation.ButtonAnima;
        }

        private void OnDisable()
        {
            this.upgradeButton.OnClickButton -= upgradeAnimation.ButtonAnima;
        }

        public void SetIcon(Sprite icon)
        {
            this.iconImage.sprite = icon;
        }

        public void SetTitle(string title)
        {
            this.titleText.text = title;
        }

        public void SetValue(string stats, string profit = null)
        {
            var text = $"Value: {stats}";
            if (profit != null)
            {
                text += $" <color=#{UPGRADE_COLOR_HEX}>(+{profit})</color>";
            }

            this.valueText.text = text;
        }

        public void SetLevel(int currentLevel, int maxLevel)
        {
            this.levelText.text = $"Level: {currentLevel}/{maxLevel}";
        }
    }
}