﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.Game.Scripts.Upgrades
{
    public class UpgradeButton : MonoBehaviour
    {
        [SerializeField]
        private Button button;
        
        [SerializeField]
        private Text price;

        [SerializeField]
        private Text max;

        [SerializeField]
        private Text upgrade;

        [SerializeField]
        private GameObject priceContainer;

        public event Action<Button> OnClickButton;
        internal void AddListener(UnityAction onButtonClicked)
        {
            this.button.onClick.AddListener(onButtonClicked);
            OnClickButton?.Invoke(button);
        }

        internal void RemoveListener(UnityAction onButtonClicked)
        {
            this.button.onClick.RemoveListener(onButtonClicked);
        }

        public void SetState(State state)
        {
            if (state == State.AVAILABLE)
            {
                button.GetComponent<Image>().color = Color.green;
            }
            else if(state == State.MAX)
            {
                button.GetComponent<Image>().color = Color.gray;
                upgrade.gameObject.SetActive(false);
                priceContainer.SetActive(false);
                max.gameObject.SetActive(true);
            }
            else if (state == State.LOCKED)
            {
                button.GetComponent<Image>().color = Color.gray;
            }
        }       
        public void SetPrice(string price)
        {
            this.price.text = price;
        }
        public enum State
        {
            AVAILABLE,
            MAX,
            LOCKED
        }
    }
}