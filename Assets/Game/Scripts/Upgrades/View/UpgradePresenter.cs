using Assets.Game.Scripts.Upgrades;
using GameSystems;
using DG.Tweening;
using UnityEngine;

namespace Assets.Game.Scripts.Upgrades
{
    public sealed class UpgradePresenter : IGameInitElement
    {
        private readonly IPlayerUpgrade upgrade;

        private readonly UpgradeView view;
        private PlayerUpgradesManager manager;
        private MoneyBank moneyBank;
        public UpgradePresenter(IPlayerUpgrade upgrade, UpgradeView view)
        {
            this.upgrade = upgrade;
            this.view = view;
        }
        void IGameInitElement.OnInitGame(IGameSystem gameSystem)
        {
            this.manager = gameSystem.GetService<PlayerUpgradesManager>();
            this.moneyBank = gameSystem.GetService<MoneyBank>();
            
            gameSystem.AddElementDuringProccess(this.manager);
        }

        public void Show()
        {
            this.view.SetTitle(this.upgrade.Title);
            this.view.SetIcon(this.upgrade.Icon);
            this.view.UpgradeButton.AddListener(this.OnButtonClicked);            
            this.upgrade.OnLevelUpEvent += this.OnLevelUp;
            this.moneyBank.OnMoneyChanged += this.OnMoneyChanged;
           
            this.UpdateLevel();
            this.UpdateButton();
        }
        
        public void Hide()
        {
            this.view.UpgradeButton.RemoveListener(this.OnButtonClicked);
            this.upgrade.OnLevelUpEvent -= this.OnLevelUp;
            this.moneyBank.OnMoneyChanged -= this.OnMoneyChanged;
        }

        private void OnMoneyChanged(int prevValue, int newValue)
        {
            this.UpdateButton();
        }

        private void OnLevelUp(int level)
        {
            this.UpdateLevel();
            this.UpdateButton();
        }

        private void OnButtonClicked()
        {
            if (this.manager.CheckUpgrade(this.upgrade)) 
            {
                this.manager.LevelUp(this.upgrade);
            }
        }

        private void UpdateLevel()
        {
            this.view.SetLevel(this.upgrade.Level, this.upgrade.MaxLevel);
        }


        private void UpdateButton()
        {
            var button = this.view.UpgradeButton;
            if (this.upgrade.IsMaxLevel())
            {
                button.SetState(UpgradeButton.State.MAX);
                return;
            }

            var price = this.upgrade.NextPrice;
            button.SetPrice(price.ToString());

            var state = this.moneyBank.Money >= price
                ? UpgradeButton.State.AVAILABLE
                : UpgradeButton.State.LOCKED;
            button.SetState(state);
        }


        
    }
}