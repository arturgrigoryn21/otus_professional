using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystems
{
    public class ServiceInstaller : MonoBehaviour
    {
        [SerializeField]
        private MonoBehaviour[] monoServices;

        [SerializeField]
        private ScriptableServiceLoader[] serviceLoaders;


        private void Awake()
        {
            LoadMonoServices();
            LoadServiceLoaders();
        }
        private void LoadMonoServices()
        {
            foreach (var service in monoServices)
            {
                ServiceLocator.AddService(service);
            }
        }

        private void LoadServiceLoaders()
        {
            foreach(var loaders in serviceLoaders)
            {
                var services = loaders.LoadServices();
                foreach(var service in services)
                    ServiceLocator.AddService(service);
            }
        }
    }
}
