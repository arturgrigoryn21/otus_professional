﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace GameSystems
{
    [CreateAssetMenu(
        fileName = "ScriptableServiceLoader",
        menuName = "GameEngine/Loader/ScriptableServiceLoader"
    )]
    class ScriptableServiceLoader : ScriptableObject
    {
        [SerializeField]
        private MonoScript[] services;

        public object[] LoadServices()
        {
            var instances = new object[services.Length];
            for (var k = 0; k < services.Length; k++)
            {
                instances[k] = CreateInstance(services[k]);
            }
            return instances;
        }

        private object CreateInstance(MonoScript script)
        {
            var type = script.GetClass();
            return Activator.CreateInstance(type);
        }
    }
}
