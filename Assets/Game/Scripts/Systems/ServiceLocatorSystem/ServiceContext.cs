using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystems
{
    public class ServiceContext : MonoBehaviour
    {
        private List<object> services = new List<object>();

        public T[] GetServices<T>()
        {
            var services = new List<T>();
            foreach (var serv in this.services)
            {
                if (serv is T result)
                    services.Add(result);
            }
            return services.ToArray();
        }

        public T GetService<T>()
        {
            foreach (var serv in services)
            {
                if (serv is T result)
                    return result;
            }
            throw new Exception("can't find service");
        }
        public bool TryGetService<T>(out T result)
        {
            foreach (var serv in services)
            {
                if (serv is T service)
                {
                    result = service;
                    return true; 
                }
            }
            result = default;
            return false;
        }

        public void AddService(object service)
        {
            services.Add(service);
        }

        public void RemoveService(object service)
        {
            if (services.Contains(service))
                services.Remove(service);
        }
    }
}
