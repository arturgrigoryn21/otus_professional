using Assets.Scripts;
using GameSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputSwitchAdapter : MonoBehaviour,
    IGameStartElement, IGameFinishElement, IGameInitElement
{
    private InputStateMachine inputStateMachine;
    private InputUIManager inputUIManager;
    public void OnFinishGame()
    {
        inputUIManager.OnSwitchInput -= SwitchInputLayer;
    }

    public void OnInitGame(IGameSystem gameSystem)
    {
        inputStateMachine = gameSystem.GetService<InputStateMachine>();
        inputUIManager = gameSystem.GetService<InputUIManager>();
    }

    public void OnStartGame()
    {
        inputUIManager.OnSwitchInput += SwitchInputLayer;
    }

    private void SwitchInputLayer()
    {
        if (inputStateMachine.CurrentState == StateInputType.Key)
            inputStateMachine.EnterState(StateInputType.Mouse);
        else if (inputStateMachine.CurrentState == StateInputType.Mouse)
            inputStateMachine.EnterState(StateInputType.Key);
    }
}
