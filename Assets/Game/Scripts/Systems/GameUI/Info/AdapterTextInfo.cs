﻿using Assets.Scripts;
using GameSystems;
using Interfeces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace GameInterfece
{
    class AdapterTextInfo : MonoBehaviour,
        IGameStartElement, IGameFinishElement, IGameInitElement
    {
        [SerializeField]
        private TextInfo text;
        private IInfoComponent infoComponent;
        public void OnFinishGame()
        {
            infoComponent.OnDistanceChanged -= PlayerInfoService_DistanceChanged;
        }

        public void OnInitGame(IGameSystem gameSystem)
        {
            var playerService = gameSystem.GetService<PlayerService>();
            infoComponent = playerService.Player.Element<IInfoComponent>();
        }

        public void OnStartGame()
        {
            infoComponent.OnDistanceChanged += PlayerInfoService_DistanceChanged;
        }

        private void PlayerInfoService_DistanceChanged(float distance)
        {
            text.SetText(distance.ToString());
        }
    }
}
