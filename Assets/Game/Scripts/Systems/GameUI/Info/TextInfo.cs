using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace GameInterfece
{
    public class TextInfo : MonoBehaviour
    {
        [SerializeField]
        private Text text;

        public void SetText(string message)
        {
            text.text = message;
        }
    }
}
