﻿using GameSystems;
using QUESTS;
using UnityEngine;

public class QuestBarAdapter : MonoBehaviour,
    IGameStartElement, IGameFinishElement, IGameInitElement
{
    [SerializeField]
    private QuestView questView;
    private QuestManager questManager;

    public void OnFinishGame()
    {
        questManager.OnTakeQuest -= SubcribeOnQuest;
        questManager.OnFinishQuest -= UnsabscribeFromQuest;
    }

    public void OnInitGame(IGameSystem gameSystem)
    {
        questManager = gameSystem.GetService<QuestService>().Manager.Element<QuestManager>();
    }

    public void OnStartGame()
    {
        questManager.OnTakeQuest += SubcribeOnQuest;
        questManager.OnFinishQuest += UnsabscribeFromQuest;
    }

    private void UnsabscribeFromQuest(IQuest quest)
    {
        quest.OnProgressChanged -= ChangeQuestProgress;
        quest.OnCompleted -= FinishQuest;
    }

    private void SubcribeOnQuest(IQuest quest)
    {
        quest.OnProgressChanged += ChangeQuestProgress;
        quest.OnCompleted += FinishQuest;
    }

    private void FinishQuest(IQuest quest)
    {
        questView.Complete();
    }

    private void ChangeQuestProgress(IQuest quest, float value)
    {
        questView.SetProgress(value);
    }
}
