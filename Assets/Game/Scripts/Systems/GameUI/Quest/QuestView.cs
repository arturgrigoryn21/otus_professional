using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestView : MonoBehaviour
{
    [SerializeField]
    private Image progress;
    
    public void SetProgress(float value)
    {
        progress.fillAmount = value;
    }

    internal void Complete()
    {
        throw new NotImplementedException();
    }
}
