using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationManager : MonoBehaviour
{
    public event Action UpdateEvent;
    void Update()
    {
        UpdateEvent?.Invoke();
    }
}
