﻿using System.Collections;
using GameSystems;

namespace Loader
{
    public class TaskInitialize : ILoadingTask
    {
        public IEnumerator Do()
        {
            var services = ServiceLocator.GetServices<IInitializable>();
            foreach (var service in services)
                service.Init();
            yield break;
        }
    }
}
