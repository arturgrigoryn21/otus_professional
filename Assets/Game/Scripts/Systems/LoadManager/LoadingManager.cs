﻿using System.Collections;
using UnityEngine;

namespace Loader
{
    public class LoadingManager : MonoBehaviour
    {
        [SerializeField]
        private LoadingConfig loadingConfig;

        private IEnumerator Start()
        {
            if (loadingConfig == null)
                yield break;
            var tasks = loadingConfig.GetTasks();
            foreach (var task in tasks)
            {
                yield return task.Do();
            }
        }
    }
}
