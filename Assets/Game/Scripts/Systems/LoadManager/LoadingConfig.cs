using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using GameSystems;

namespace Loader
{
    [CreateAssetMenu(
        fileName = "LoadingConfig",
        menuName = "GameEngine/Loader/LoadingConfig"
    )]
    public class LoadingConfig : ScriptableObject
    {

        [SerializeField]
        private MonoScript[] tasks;

        public IEnumerable<ILoadingTask> GetTasks()
        {
            var instances = new List<ILoadingTask>();
            foreach (var task in this.tasks)
            {
                var instance = Activator.CreateInstance(task.GetClass()) as ILoadingTask;
                instances.Add(instance);
            }
            return instances;
        }
    }
}
