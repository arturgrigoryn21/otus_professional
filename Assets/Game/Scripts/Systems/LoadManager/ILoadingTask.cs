﻿using System.Collections;

namespace Loader
{
    public interface ILoadingTask
    {
        IEnumerator Do();
    }
}
