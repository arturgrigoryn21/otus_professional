﻿using QUESTS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace GameSystems
{
    public interface IGameSystem
    {
        void InitGame();

        void StartGame();

        void PauseGame();

        void ResumeGame();

        void FinishGame();

        T GetService<T>();
        void AddElementDuringProccess(object gameElement);
        void RemoveElement(IGameElement element);
    }
}
