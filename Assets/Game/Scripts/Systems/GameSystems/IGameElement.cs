﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSystems
{
    public interface IGameElement 
    { 
    }
    public interface IGameInitElement : IGameElement
    {
        void OnInitGame(IGameSystem gameSystem);
    }

    public interface IGameStartElement : IGameElement
    {
        void OnStartGame();
    }

    public interface IGameFinishElement : IGameElement
    {
        void OnFinishGame();
    }

    public interface IGamePauseElement : IGameElement
    {
        void OnPauseGame();
    }

    public interface IGameResumeElement : IGameElement
    {
        void OnResumeGame();
    }
}
