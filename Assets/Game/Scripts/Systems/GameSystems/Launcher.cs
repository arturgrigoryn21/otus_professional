using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystems
{
    public class Launcher : MonoBehaviour
    {
        [SerializeField]
        private MonoGameSystem gameSystem;
        void Start()
        {
            gameSystem.InitGame();
            gameSystem.StartGame();
        }
    }
}
