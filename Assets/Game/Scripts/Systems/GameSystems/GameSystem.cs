using QUESTS;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystems
{
    public class GameSystem : IGameSystem
    {
        private readonly List<IGameElement> elements = new List<IGameElement>();
        private readonly List<IGameElement> elementsCache = new List<IGameElement>();

        private readonly List<MonoBehaviour> services = new List<MonoBehaviour>();

        public void AddElement(IGameElement gameElement)
        {
            elements.Add(gameElement);            
        }

        public T GetService<T>()
        {
            foreach (var serv in services)
            {
                if (serv is T result)
                    return result;
            }
            throw new Exception($"can't find service: {typeof(T).Name}");
        }

        internal void AddService(MonoBehaviour serv)
        {
            services.Add(serv);
        }

        public void InitGame()
        {
            UpdateCache();
            foreach (var service in elementsCache)
            {
                if (service is IGameInitElement component)
                    component.OnInitGame(this);
            }
        }

        

        public void FinishGame()
        {
            UpdateCache();
            foreach (var service in elementsCache)
            {
                if (service is IGameFinishElement component)
                    component.OnFinishGame();
            }
        }


        public void PauseGame()
        {
            UpdateCache();
            foreach (var service in elementsCache)
            {
                if (service is IGamePauseElement component)
                    component.OnPauseGame();
            }
        }

        public void ResumeGame()
        {
            UpdateCache();
            foreach (var service in elementsCache)
            {
                if (service is IGameResumeElement component)
                    component.OnResumeGame();
            }
        }

        public void StartGame()
        {
            UpdateCache();
            foreach (var service in elementsCache)
            {
                if (service is IGameStartElement component)
                    component.OnStartGame();
            }
        }

        public void RemoveElement(IGameElement element)
        {
            if(elements.Contains(element))
                elements.Remove(element);
        }

        private void UpdateCache()
        {
            elementsCache.Clear();
            elementsCache.AddRange(elements);
        }

        public void AddElementDuringProccess(object gameElement)
        {
            if(gameElement is IGameElement)
                elements.Add((IGameElement)gameElement);

            if (gameElement is IGameInitElement element)
                element.OnInitGame(this);

            if (gameElement is IGameStartElement element1)
                element1.OnStartGame();
        }
    }
}
