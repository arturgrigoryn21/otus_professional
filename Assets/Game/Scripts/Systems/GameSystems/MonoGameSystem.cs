﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace GameSystems
{
    [AddComponentMenu("GameSystems/Game System")]
    class MonoGameSystem : MonoBehaviour, IGameSystem, ISerializationCallbackReceiver
    {
        [SerializeField]
        private List<MonoBehaviour> elements = new List<MonoBehaviour>();

        [SerializeField]
        private List<MonoBehaviour> services = new List<MonoBehaviour>();

        private readonly GameSystem gameSystem = new GameSystem();


        private void AddService()
        {
            foreach (var serv in services)
            {
                gameSystem.AddService(serv);
            }
        }

        private void AddElements()
        {
            foreach (var elem in elements)
            {
                if (elem is IGameElement element)
                    gameSystem.AddElement(element);
            }
        }
        public void FinishGame()
        {
            gameSystem.FinishGame();
        }

        public T GetService<T>()
        {
            return gameSystem.GetService<T>();
        }

        public void InitGame()
        {
            gameSystem.InitGame();
        }

        public void PauseGame()
        {
            gameSystem.InitGame();
        }

        public void ResumeGame()
        {
            gameSystem.ResumeGame();
        }

        public void StartGame()
        {
            gameSystem.StartGame();
        }

        public void OnBeforeSerialize()
        {

        }

        public void OnAfterDeserialize()
        {
            AddElements();
            AddService();
        }

        public void AddElement(IGameElement gameElement)
        {
            gameSystem.AddElement(gameElement);
        }

        public void AddService(MonoBehaviour service)
        {
            gameSystem.AddService(service);
        }

        public void RemoveElement(IGameElement element)
        {
            gameSystem.RemoveElement(element);
        }

        public void AddElementDuringProccess(object gameElement)
        {
            gameSystem.AddElementDuringProccess(gameElement);
        }
    }
}
