﻿using Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Game.Scripts.Inventory
{
    public interface IInventory : IEnumerable<IInventoryItem>
    {
        event Action<IInventoryItem> OnAddItem;
        event Action<IInventoryItem> OnRemoveItem;
        void AddItem(IInventoryItem inventoryItem);
        void RemoveItem(IInventoryItem item);
        void RemoveItem(string itemName);
        bool TryGetItem(InventoryItemType type, out IInventoryItem inventoryItem);
    }
}
