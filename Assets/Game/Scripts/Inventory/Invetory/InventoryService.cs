﻿using Entities;
using UnityEngine;

namespace Assets.Game.Scripts.Inventory
{
    public class InventoryService : MonoBehaviour
    {
        [SerializeField]
        private MonoEntity inventory;

        public MonoEntity Inventory => inventory;
    }
}
