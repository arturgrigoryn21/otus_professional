﻿using GameSystems;
using UnityEngine;
using System.Linq;

namespace Assets.Game.Scripts.Inventory.Invetory
{
    public class InventoryCountDecorator : MonoBehaviour
    {
        [SerializeField]
        private InventoryItemCatalog catalog;

        public int CountItems(IInventory inventory, string itemName)
        {
            if (!catalog.TryGetItem(itemName, out InventoryItem prefab))
                throw new System.Exception("can't find prefab");

            if ((prefab.ItemType &  InventoryItemType.STACKABLE) == InventoryItemType.STACKABLE)
            {
                return this.CountItemsAsStackable(inventory, itemName);
            }

            return this.CountItemsAsSingle(inventory, itemName);
        }

        private int CountItemsAsSingle(IInventory inventory, string itemName)
        {
            return inventory.Count(x => x.Name == itemName);
        }

        private int CountItemsAsStackable(IInventory inventory, string itemName)
        {
            var result = 0;

            var items = inventory.Where(x => x.Name == itemName); 
            foreach(var item in items)
            {
                var itemCount = item.Element<StackableComponent>().Value;
                result += itemCount;
            }
            return result;
        }
    }

}
