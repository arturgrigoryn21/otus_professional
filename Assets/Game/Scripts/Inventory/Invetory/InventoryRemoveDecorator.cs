﻿using GameSystems;
using System;
using System.Linq;
using UnityEngine;

namespace Assets.Game.Scripts.Inventory.Invetory
{

    public class InventoryRemoveDecorator : MonoBehaviour, IGameInitElement
    {
        private InventoryItemFactory factory;
        public void RemoveItems(IInventory inventory, string itemName, int count)
        {
            for (var i = 0; i < count; i++)
            {
                if (!RemoveItem(inventory, itemName))
                    return;
            }
        }

        public bool RemoveItem(IInventory inventory, string itemName)
        {
            var items = inventory.Where(x => x.Name == itemName);
            if (items.Count() <= 0)
            {
                return false;
            }

            var targetItem = items.Last();
            if ((targetItem.ItemType & InventoryItemType.STACKABLE) == InventoryItemType.STACKABLE)
            {
                this.RemoveAsStackable(inventory, targetItem);
            }
            else
            {
                this.RemoveAsSingle(inventory, targetItem);
            }

            return true;
        }

        private void RemoveAsStackable(IInventory inventory, IInventoryItem item)
        {
            var stackableComponent = item.Element<StackableComponent>();
            stackableComponent.Decrement(1);

            if (stackableComponent.Value > 0)
            {
                return;
            }

            inventory.RemoveItem(item);

            if (item is InventoryItem monoItem)
            {
                factory.DestroyItem(monoItem);
            }
        }

        private void RemoveAsSingle(IInventory inventory, IInventoryItem item)
        {
            inventory.RemoveItem(item);

            if (item is InventoryItem monoItem)
            {
                this.factory.DestroyItem(monoItem);
            }
        }

        void IGameInitElement.OnInitGame(IGameSystem gameSystem)
        {
            factory = gameSystem.GetService<InventoryItemFactory>();
        }
    }

}
