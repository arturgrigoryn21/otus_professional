﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Game.Scripts.Inventory
{
    public class Inventory : MonoBehaviour, IInventory
    {
        public event Action<IInventoryItem> OnAddItem;
        public event Action<IInventoryItem> OnRemoveItem;

        private readonly List<IInventoryItem> inventoryItems = new List<IInventoryItem>();

        public void AddItem(IInventoryItem inventoryItem)
        {
            if(!inventoryItems.Contains(inventoryItem))
            {
                inventoryItems.Add(inventoryItem);
                OnAddItem?.Invoke(inventoryItem);
            }           
        }

        public bool TryGetItem(InventoryItemType type, out IInventoryItem inventoryItem)
        {
            foreach (var item in inventoryItems)
            {
                if ((item.ItemType & type) == type)
                {
                    inventoryItem = item;                    
                    return true;
                }
            }
            inventoryItem = null;
            return false;
        }

        
        public void RemoveItem(IInventoryItem inventoryItem)
        {
            if (inventoryItems.Contains(inventoryItem))
            {
                inventoryItems.Remove(inventoryItem);
                OnRemoveItem?.Invoke(inventoryItem);
            }
        }

        public void RemoveItem(string name)
        {
            if (inventoryItems.Any(x => x.Name == name))
            {
                var item = inventoryItems.First(x => x.Name == name);
                RemoveItem(item);
            }
        }

        public bool TryGetItem(string name, out IInventoryItem inventoryItem)
        {
            inventoryItem = inventoryItems.FirstOrDefault(x => x.Name == name);
            return inventoryItem != null;
        }     

        public IEnumerator<IInventoryItem> GetEnumerator()
        {
            foreach(var item in inventoryItems)
                yield return item;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
