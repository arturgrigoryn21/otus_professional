﻿using GameSystems;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Game.Scripts.Inventory.Invetory
{

    public class InventoryAddDecorator : MonoBehaviour, IGameInitElement
    {
        private InventoryItemFactory factory;

        [SerializeField]
        private InventoryItemCatalog catalog;

        public void AddItems(IInventory inventory, string itemName, int count)
        {
            for (var i = 0; i < count; i++)
            {
                AddItem(inventory, itemName);
            }
        }

        public void AddItem(IInventory inventory, string itemName)
        {
            if (!catalog.TryGetItem(itemName, out InventoryItem prefab))
                return;

            if ((prefab.ItemType & InventoryItemType.STACKABLE) == InventoryItemType.STACKABLE)
            {
                AddAsStackable(inventory, prefab);
            }
            else
            {
                AddAsSingle(inventory, prefab);
            }
        }

        private void AddAsStackable(IInventory inventory, InventoryItem prefab)
        {
            var items = inventory
                .Where(x => x.Name == prefab.Name)
                .Where(it => !it.Element<StackableComponent>().IsLimit)
                .ToArray();

            IInventoryItem item;
            if (items.Length <= 0)
            {
                item = factory.CreateItem(prefab);
                item.Element<StackableComponent>().Increment(1);
                inventory.AddItem(item);
                return;
            }

            item = items[0];
            item.Element<StackableComponent>().Increment(1);
        }

        private void AddAsSingle(IInventory inventory, InventoryItem prefab)
        {
            var item = factory.CreateItem(prefab);
            inventory.AddItem(item);
        }

        void IGameInitElement.OnInitGame(IGameSystem gameSystem)
        {
            factory = gameSystem.GetService<InventoryItemFactory>();
        }
    }

}
