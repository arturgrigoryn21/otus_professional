﻿using System;

namespace Assets.Game.Scripts.Inventory
{
    [Flags]
    public enum InventoryItemType
    {
        NONE = 0b0000,
        STACKABLE = 0b0001,
        CONSUMABLE = 0b0010,
        EQUPPABLE = 0b0100,
        EFFECTIBLE = 0b1000
    }
}
