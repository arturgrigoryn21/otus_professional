﻿using Entities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Game.Scripts.Inventory
{
    public class InventoryItem : MonoEntity, IInventoryItem
    {
        [SerializeField]
        private string _name;

        [SerializeField]
        private InventoryItemType _type;
        public string Name => _name;
        public InventoryItemType ItemType => _type;
    }
}
