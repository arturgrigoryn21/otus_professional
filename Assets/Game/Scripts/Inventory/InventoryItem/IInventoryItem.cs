﻿using Entities;

namespace Assets.Game.Scripts.Inventory
{
    public interface IInventoryItem : IEntity
    {
        string Name { get; }
        InventoryItemType ItemType { get; }
    }
}
