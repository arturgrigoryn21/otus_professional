﻿using UnityEngine;

namespace Assets.Game.Scripts.Inventory
{
    public class InventoryItemFactory : MonoBehaviour
    {
        [SerializeField]
        private Transform itemContainer;

        public IInventoryItem CreateItem(InventoryItem prefab)
        {
            return Instantiate(prefab, itemContainer);
        }

        public void DestroyItem(InventoryItem item)
        {
            Destroy(item.gameObject);
        }

    }
}
