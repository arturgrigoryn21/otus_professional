﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Game.Scripts.Inventory
{
    [CreateAssetMenu(fileName ="ItemCatalog", menuName = "GameEngine/ItemCatalog")]
    internal class InventoryItemCatalog : ScriptableObject, IEnumerable<InventoryItem>
    {
        [SerializeField] 
        private List<InventoryItem> inventoryItems = new List<InventoryItem>();


        public bool TryGetItem(string name, out InventoryItem item)
        {
            item = inventoryItems.FirstOrDefault(x => x.Name == name);
            return item != null;
        }

        public IEnumerator<InventoryItem> GetEnumerator()
        {
            foreach (var item in inventoryItems)
                yield return item;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
