﻿using Assets.Game.Scripts.Inventory.Invetory;
using GameSystems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Game.Scripts.Inventory
{
    internal class DebugInventory : MonoBehaviour, IGameInitElement
    {
        [SerializeField]
        private string itemName;

        [SerializeField]
        private int number;

        private Inventory inventory;

        private InventoryAddDecorator addDecorator;
        private InventoryRemoveDecorator removeDecorator;
        private InventoryCountDecorator countDecorator;


        void IGameInitElement.OnInitGame(IGameSystem gameSystem)
        {
            inventory = gameSystem.GetService<InventoryService>().Inventory.Element<Inventory>();
            addDecorator = gameSystem.GetService<InventoryAddDecorator>();
            removeDecorator = gameSystem.GetService<InventoryRemoveDecorator>();
            countDecorator = gameSystem.GetService<InventoryCountDecorator>();
        }

        [ContextMenu("AddItem")]
        public void AddItem()
        {
            addDecorator.AddItems(inventory, itemName, number);
        }



        [ContextMenu("RemoveItem")]
        public void RemoveItem()
        {
            removeDecorator.RemoveItems(inventory, itemName, number);
        }

        [ContextMenu("CountItems")]
        public void CountItem()
        {
            Debug.Log(countDecorator.CountItems(inventory, itemName));
        }
    }
}
