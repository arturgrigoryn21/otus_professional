﻿using UnityEngine;

namespace Assets.Game.Scripts.Inventory
{
    public class ConsumableComponent : MonoBehaviour
    {
        public void Eat()
        {
            Debug.Log("Ate");
        }
    }
}
