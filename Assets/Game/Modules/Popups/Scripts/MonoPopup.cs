﻿using System;
using UnityEngine;

public abstract class MonoPopup : MonoBehaviour, IPopup
{
    public event Action<PopupName> OnClosePopup;
    [SerializeField]
    protected PopupName popupName;
    public PopupName PopupName => this.popupName;

    public virtual void Hide()
    {
        OnClosePopup?.Invoke(popupName);
        gameObject.SetActive(false);
    }

    public virtual void Show(IPopupArgs popupArgs)
    {
        this.gameObject.SetActive(true);
    }

   
}



