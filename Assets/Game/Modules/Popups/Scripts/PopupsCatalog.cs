﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Popups
{
    [CreateAssetMenu(
        fileName = "PopupsCatalog",
        menuName = "GameEngine/UI/PopupCatalog"
    )]
    class PopupsCatalog : ScriptableObject
    {
        [SerializeField]
        private MonoPopup[] popups;

        
        public MonoPopup GetPopup(PopupName popupName)
        {
            if (!popups.Any(x => x.PopupName == popupName))
                throw new Exception($"can't find popup: {popupName}");
            var popup = popups.First(x => x.PopupName == popupName);
            return popup;
        }

        
    }
}
