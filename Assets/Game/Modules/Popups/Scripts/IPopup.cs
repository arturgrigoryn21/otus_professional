﻿using System;

public interface IPopup
{
    event Action<PopupName> OnClosePopup;
    public PopupName PopupName { get; }
    void Show(IPopupArgs popupArgs);
    void Hide();
}


public interface IPopupArgs
{

}
