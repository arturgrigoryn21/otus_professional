using Assets.Scripts.Popups;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupManager : MonoBehaviour
{
    public event Action OnAnyPopupShown;
    public event Action OnAllPopupHidden;
    public event Action<PopupName> OnPopupHidden;

    private Dictionary<PopupName, IPopup> activePopup = new Dictionary<PopupName, IPopup>();

    [SerializeField]
    private PopupFabrica popupFabrica;


    public void Show(PopupName popupName, IPopupArgs popupArgs = null)
    {
        if (IsPopupActive(popupName))
            return;

        var popup = popupFabrica.GetPopup(popupName);
        popup.Show(popupArgs);
        activePopup.Add(popupName, popup);
        popup.OnClosePopup += Hide;
        CheckActivePopups();
    }



    public void Hide(PopupName popupName)
    {
        if (!IsPopupActive(popupName))
            return;

        var popup = activePopup[popupName];
        popup.OnClosePopup -= Hide;
        activePopup.Remove(popupName);
        popup.Hide();

        OnPopupHidden?.Invoke(popup.PopupName);
        CheckActivePopups();
    }

    private bool IsPopupActive(PopupName popupName)
    {
        return activePopup.ContainsKey(popupName);
    }

    private void CheckActivePopups()
    {
        if (activePopup.Count == 1)
            OnAnyPopupShown?.Invoke();
        else if (activePopup.Count == 0)
            OnAllPopupHidden?.Invoke();
    }
}





