using Assets.Scripts.Popups;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupFabrica : MonoBehaviour
{
    [SerializeField]
    private Transform popupsContainer; 

    [SerializeField]
    private PopupsCatalog popupsCatalog;

    public Dictionary<PopupName, IPopup> cache = new Dictionary<PopupName, IPopup>();

    public IPopup GetPopup(PopupName popupName)
    {
        if(cache.TryGetValue(popupName, out IPopup popup))
        {
            return popup;
        }
        else
        {
            popup = Instantiate(popupsCatalog.GetPopup(popupName), popupsContainer);
            cache.Add(popupName, popup);
            return popup;
        }
    }
}
